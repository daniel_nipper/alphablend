#version 400
in layout(location=0) vec3 position;
in layout(location=1) vec4 Color;
in layout(location=2) vec3 normal;
in layout(location=3) vec2 uv;

uniform mat4 transform;

out vec2 inUv;
out vec4 deColor;


void main()
{
inUv=uv;
vec4 p=vec4(position,1);
vec4 newPosition=transform*p;
gl_Position=newPosition;
deColor=Color;
	
}