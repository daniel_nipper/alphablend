#version 400

in vec4 deColor;
in vec2 inUv;
uniform sampler2D renderdtex;
out vec4 theFinalColor;

void main()
{
	
	theFinalColor = texture(renderdtex,inUv).rgba ;
	if(theFinalColor.a==0)
	discard;
};