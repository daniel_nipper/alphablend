#version 400


in vec2 inUv;
in vec3 inPosition;
in vec4 shadowUv;
in vec3 inNormal;
uniform vec3 ambientLight;
uniform vec3 lightPosition;
uniform vec3 eyePosition;
uniform mat4 ModelMatrix;
uniform sampler2D renderdtex;

out vec4 theFinalColor;

void main()
{
float power=15.0f;
	vec3 specCol=vec3(1.0f,1.0f,1.0f);
		vec3 lightColor=vec3(1.0f,1.0f,1.0f);
	vec4 shadCord=shadowUv/shadowUv.w;
	shadCord=vec4(shadCord.x+1/2,shadCord.y+1/2,shadCord.z+1/2,shadCord.w);

	vec3 tranNorm=mat3(ModelMatrix)*inNormal;
	vec3 tranposNorm=vec3(ModelMatrix*vec4(inPosition,1));
	vec3 lightvector=normalize(lightPosition-tranposNorm);
	float brightness=dot(lightvector,tranNorm);
	float brightness2=clamp(brightness,0,1);
	
	vec3 normeye=normalize(eyePosition-tranposNorm);
	
	vec3 nextColor=lightColor*brightness2;
	vec3 reflec=-reflect(lightvector,inNormal);
	float dotResult= dot(reflec,normeye);
	float sepcbright=clamp(dotResult,0,1);
	float spec=pow(sepcbright,power);
	vec3 newSpecCol=specCol*spec;

float meResult=texture(renderdtex,shadCord.xy).x;
if(meResult<shadCord.z-.005)
{
	theFinalColor = vec4(0.0f,0.0f,0.0f,1);
}
else
{
	theFinalColor = vec4((nextColor)+newSpecCol,1);
	}
	

	
};
