#version 400

in vec4 deColor;
in vec2 inUv;

out vec4 theFinalColor;

void main()
{
	
	theFinalColor = deColor;
	
};