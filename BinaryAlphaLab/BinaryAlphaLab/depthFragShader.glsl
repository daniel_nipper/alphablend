#version 400

in vec4 deColor;
in vec2 inUv;
uniform sampler2D renderdtex;

out vec4 theFinalColor;

void main()
{
	vec4 temp=texture(renderdtex,inUv).rgba ;
	theFinalColor =vec4(pow(temp.x,120),pow(temp.y,120),pow(temp.z,120),1.0f);
	
};