#version 400
in layout(location=0) vec3 position;
in layout(location=1) vec3 Tangent;
in layout(location=2) vec3 normal;
in layout(location=3) vec2 uv;


uniform mat4 transform;

uniform mat4 modelToWorld;
out mat3 tangentMat;
out vec3 inPosition;
out vec2 inUv;
void main()
{
	inUv=uv;
	inPosition=position;
	vec4 p=vec4(position,1);
	vec4 newPosition=transform*p;
	gl_Position=newPosition;
	
	vec3 tempNorm=mat3(modelToWorld)*normal;
	vec3 tempTan=mat3(modelToWorld)*Tangent;
	vec3 result =cross(tempNorm,tempTan);
	tangentMat=mat3(tempTan,result,tempNorm);
}