#version 400
in layout(location=0) vec3 position;
in layout(location=1) vec4 Color;
in layout(location=2) vec3 normal;
in layout(location=3) vec2 uv;

uniform vec3 ambientLight;
uniform mat4 transform;
uniform vec3 lightPosition;
uniform float power;
uniform vec3 specCol;
uniform vec3 eyePosition;
uniform mat4 modelToWorld;
uniform vec3 lightColor;
uniform sampler2D renderdtex;

out vec4 deColor;
out vec3 inPosition;
out vec2 inUv;
void main()
{
	inUv=uv;
	inPosition=position;
	vec4 p=vec4(position,1);
	vec4 newPosition=transform*p;
	gl_Position=newPosition;
	
	deColor=vec4(Color);
	
}