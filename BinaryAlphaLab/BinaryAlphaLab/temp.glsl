#version 400


	in layout(location=0) vec3 position;
	in layout(location=1) vec3 Color;
	in layout(location=2) vec3 normal;
	in layout(location=3) vec2 uv;
	out vec3 ReflectDir; 
	
	uniform float DrawSkyBox; 
	uniform vec3 WorldCameraPosition;
	uniform mat4 ModelViewMatrix;
	uniform mat4 ModelMatrix;
	uniform mat4 ProjectionMatrix;
	uniform mat4 transform;
	void main()
	{
		if(DrawSkyBox==1) 
			ReflectDir = position;
		
		else {
			
			vec3 worldPos = vec3( ModelMatrix *vec4(position,1.0) );
		
			vec3 worldNorm = vec3(ModelMatrix *vec4(normal, 0.0));
	
			vec3 worldView = normalize( WorldCameraPosition-worldPos);
		
			ReflectDir = reflect(-worldView, worldNorm );
		}
		gl_Position = transform * vec4(position,1.0);
	}