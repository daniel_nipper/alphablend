#include <QtGui\qapplication.h>
#include "BinaryAlphaScene.h"
#include "..\..\GameSolution\Engine\MasteWidge.h"

int main(int args,char*argv[])
{
	QApplication theApp(args,argv);
	
	BinaryAlphaScene *theAlpha=new BinaryAlphaScene();
	theAlpha->setMinimumHeight(700);
	theAlpha->setMinimumWidth(400);
	MasteWidge mw(theAlpha);
	mw.showMaximized();
	return theApp.exec();
}