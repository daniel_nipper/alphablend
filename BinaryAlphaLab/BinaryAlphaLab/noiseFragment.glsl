#version 400
#define PI 3.14159265
uniform sampler2D NoiseTex;

uniform float Octave;
in vec2 inUv;
out vec4 FragColor;
void main()
{
int temp= int(Octave);
 vec4 noise = texture(NoiseTex, inUv);
 
 FragColor = vec4( noise[temp],noise[temp],noise[temp], 1.0 );
}