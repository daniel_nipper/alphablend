#version 400

in vec4 deColor;
in vec2 inUv;
uniform sampler2D renderdtex;
uniform vec4 solidColor;
out vec4 theFinalColor;

void main()
{
	
	theFinalColor = texture(renderdtex,inUv).rgba ;
	theFinalColor=theFinalColor+solidColor;
	
};