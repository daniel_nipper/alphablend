#version 400

in vec4 deColor;
in vec2 inUv;
in vec3 inPosition;
uniform vec3 ambientLight;
uniform vec3 lightPosition;
uniform float power;
uniform vec3 specCol;
uniform vec3 eyePosition;
uniform mat4 modelToWorld;
uniform vec3 lightColor;
uniform sampler2D renderdtex;

out vec4 theFinalColor;

void main()
{
	vec3 meNorm=(((texture(renderdtex,inUv).rgb)-.5)*2);
	vec3 tranNorm=meNorm;
	vec3 tranposNorm=vec3(modelToWorld*vec4(inPosition,1));
	vec3 lightvector=normalize(lightPosition-tranposNorm);
	float brightness=dot(lightvector,tranNorm);
	float brightness2=clamp(brightness,0,1);
	
	vec3 normeye=normalize(eyePosition-tranposNorm);
	
	vec3 nextColor=lightColor*brightness2;
	vec3 reflec=-reflect(lightvector,meNorm);
	float dotResult= dot(reflec,normeye);
	float sepcbright=clamp(dotResult,0,1);
	float spec=pow(sepcbright,power);
	vec3 newSpecCol=specCol*spec;

	theFinalColor = vec4((nextColor)+newSpecCol,1);
	

	
};