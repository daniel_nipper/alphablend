#include <GL\glew.h>
#pragma once

#include <QtOpenGL\qglwidget>
#include <Qt\qtimer.h>
#include <QtGui\qmouseevent>
#include <QtGui\qkeyevent>
#include "..\..\zExternalDependencies\glm\glm\glm.hpp"
#include "..\..\NeumontTools\include\ShapeGenerator.h"
#include "..\..\GameSolution\Engine\GeneralWindows.h"
#include "..\..\GameSolution\Engine\DebugMenu.h"
class BinaryAlphaScene: public QGLWidget
{
	/*if(theFinalColor.a==0)
	discard;
	cool stuff
	float t = (cos( noise[1] * PI ) + 1.0) / 2.0;
 vec4 color = mix( SkyColor, CloudColor, t );
 uniform vec4 SkyColor = vec4( 0.3, 0.0, 0.0, 1.0 );
uniform vec4 CloudColor = vec4( 1.0, 1.0, 1.0, 1.0 );
	*/
	Q_OBJECT
	QTimer myTimer;
public:
	void initializeGL();
	void paintGL();
	void makeCube();
	void makeNoiseTex();
	void setUpFrameBuffer();
	void drawStuff();
	void mouseMoveEvent(QMouseEvent*);
	void keyPressEvent(QKeyEvent*);
	private slots:
	void myUpdate();
};

