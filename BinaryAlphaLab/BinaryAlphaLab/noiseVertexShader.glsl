#version 400
in layout(location=0) vec3 position;
in layout(location=1) vec3 Color;
in layout(location=2) vec3 normal;
in layout(location=3) vec2 uv;

uniform mat4 transform;
uniform sampler2D NoiseTex;
uniform float Octave;
uniform float Alpha;
out vec2 inUv;
out vec4 deColor;


void main()
{
inUv=uv;
int temp= int(Octave);
 vec4 noise = texture(NoiseTex, inUv);
vec4 p=vec4(position.x,noise[temp]*10,position.z,1);
vec4 newPosition=transform*p;
gl_Position=newPosition;
deColor=vec4(Color,Alpha);
	
}