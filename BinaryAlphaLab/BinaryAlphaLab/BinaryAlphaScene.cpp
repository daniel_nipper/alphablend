#include "BinaryAlphaScene.h"
#include "..\..\zExternalDependencies\glm\glm\gtx\compatibility.hpp"
#include "..\..\zExternalDependencies\glm\glm\gtc\matrix_transform.hpp"
#include "..\..\zExternalDependencies\glm\glm\gtx\transform.hpp"
#include "..\..\GameSolution\Engine\Engine.h"
#include "..\..\GameSolution\Engine\Camera.h"
#include "..\..\GameSolution\Engine\DebugShapes.h"
#include <Qt\qdebug.h>
#include "..\..\GameSolution\Engine\FastDelegate.h"
#include <noise\noise.h>
Camera theCamera;
using glm::mat4;
GLuint myFrameBuffer;
GLuint myColorTextureID;
GLuint myDepthTextureID;
GeneralWindows* genWindow=new GeneralWindows();
GeneralWindows::RenderableInfo* theRend[20];
GeneralWindows::GeometryInfo* thegeom[20];
GeneralWindows::TextureInfo* thetexture[20];
GeneralWindows::ShaderInfo* theShadeInfo[20];
DebugShapes thebug;
float xAxis=2.0f;
float yAxis=0.0f;
float zAxis=3.5f;
float spec=25.0f;
bool visible;
mat4 planeTransform;
mat4 cubeTransform;
mat4 customCubeTransform;
mat4 sphereTransform;
float inAlpha;
bool Alpha_Blending;
mat4 planeNormTransform;
mat4 theLightTransform;
bool show_plane;
bool show_Cube;
float x_Rotation;
float y_Rotation;
float threshHold;
GLuint texID;
float noiseSlider;
float MapOneOrTwo;
struct Vertex
{
public :glm::vec3 Position;
public: glm::vec3 Tangent;
public :glm::vec3 Normal;
public :glm::vec2 uv;

};
void BinaryAlphaScene::initializeGL()
{
	//23.25
	
	glClearColor(.9f,.7f,.5f,1.0f);
	theCamera.position=glm::vec3(5.6f,10.7f,24.95f);
	setMouseTracking(true);
	glewInit();
	setUpFrameBuffer();
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	
	thetexture[0]= genWindow->addTexture("Energy_Sword.png");
	Neumont::ShapeData thePlane=Neumont::ShapeGenerator::makePlane(27);
	thegeom[0]=genWindow->addGeometry(thePlane.verts,thePlane.vertexBufferSize(),thePlane.indexBufferSize(),thePlane.indices,thePlane.numIndices,GL_TRIANGLES);
	theShadeInfo[0]= genWindow->createShaderInfo("noLightingVertexShader.glsl","FragmentShader.glsl");
	genWindow->addShaderStreamedParameter(thegeom[0],0,GeneralWindows::ParameterType::PT_VEC3,Neumont::Vertex::STRIDE,Neumont::Vertex::POSITION_OFFSET);
	genWindow->addShaderStreamedParameter(thegeom[0],1,GeneralWindows::ParameterType::PT_VEC4,Neumont::Vertex::STRIDE,Neumont::Vertex::COLOR_OFFSET);
	genWindow->addShaderStreamedParameter(thegeom[0],2,GeneralWindows::ParameterType::PT_VEC3,Neumont::Vertex::STRIDE,Neumont::Vertex::NORMAL_OFFSET);
	genWindow->addShaderStreamedParameter(thegeom[0],3,GeneralWindows::ParameterType::PT_VEC2,Neumont::Vertex::STRIDE,Neumont::Vertex::UV_OFFSET);
	theRend[0]=genWindow->addRenderable(thegeom[0],planeTransform,theShadeInfo[0],thetexture[0]);
	
	thetexture[1]= genWindow->addTexture("dragon_PNG1603.png");
	Neumont::ShapeData theCube=Neumont::ShapeGenerator::makePlane(10);
	thegeom[1]=genWindow->addGeometry(theCube.verts,theCube.vertexBufferSize(),theCube.indexBufferSize(),theCube.indices,theCube.numIndices,GL_TRIANGLES);
	theShadeInfo[1]= genWindow->createShaderInfo("noLightingVertexShader.glsl","FragmentShader.glsl");
	genWindow->addShaderStreamedParameter(thegeom[1],0,GeneralWindows::ParameterType::PT_VEC3,Neumont::Vertex::STRIDE,Neumont::Vertex::POSITION_OFFSET);
	genWindow->addShaderStreamedParameter(thegeom[1],1,GeneralWindows::ParameterType::PT_VEC4,Neumont::Vertex::STRIDE,Neumont::Vertex::COLOR_OFFSET);
	genWindow->addShaderStreamedParameter(thegeom[1],2,GeneralWindows::ParameterType::PT_VEC3,Neumont::Vertex::STRIDE,Neumont::Vertex::NORMAL_OFFSET);
	genWindow->addShaderStreamedParameter(thegeom[1],3,GeneralWindows::ParameterType::PT_VEC2,Neumont::Vertex::STRIDE,Neumont::Vertex::UV_OFFSET);
	theRend[1]=genWindow->addRenderable(thegeom[1],cubeTransform,theShadeInfo[1],thetexture[1]);

	Neumont::ShapeData theSphere=Neumont::ShapeGenerator::makeSphere(12);
	thegeom[2]=genWindow->addGeometry(theSphere.verts,theSphere.vertexBufferSize(),theSphere.indexBufferSize(),theSphere.indices,theSphere.numIndices,GL_TRIANGLES);
	theShadeInfo[2]= genWindow->createShaderInfo("noLightingVertexShader.glsl","noTextureFragmentShader.glsl");
	genWindow->addShaderStreamedParameter(thegeom[2],0,GeneralWindows::ParameterType::PT_VEC3,Neumont::Vertex::STRIDE,Neumont::Vertex::POSITION_OFFSET);
	genWindow->addShaderStreamedParameter(thegeom[2],1,GeneralWindows::ParameterType::PT_VEC4,Neumont::Vertex::STRIDE,Neumont::Vertex::COLOR_OFFSET);
	genWindow->addShaderStreamedParameter(thegeom[2],2,GeneralWindows::ParameterType::PT_VEC3,Neumont::Vertex::STRIDE,Neumont::Vertex::NORMAL_OFFSET);
	genWindow->addShaderStreamedParameter(thegeom[2],3,GeneralWindows::ParameterType::PT_VEC2,Neumont::Vertex::STRIDE,Neumont::Vertex::UV_OFFSET);
	theRend[2]=genWindow->addRenderable(thegeom[2],sphereTransform,theShadeInfo[2],thetexture[1]);

	Neumont::ShapeData theLight=Neumont::ShapeGenerator::makeCube();
	thegeom[3]=genWindow->addGeometry(theLight.verts,theLight.vertexBufferSize(),theLight.indexBufferSize(),theLight.indices,theLight.numIndices,GL_TRIANGLES);
	genWindow->addShaderStreamedParameter(thegeom[3],0,GeneralWindows::ParameterType::PT_VEC3,Neumont::Vertex::STRIDE,Neumont::Vertex::POSITION_OFFSET);
	genWindow->addShaderStreamedParameter(thegeom[3],1,GeneralWindows::ParameterType::PT_VEC4,Neumont::Vertex::STRIDE,Neumont::Vertex::COLOR_OFFSET);
	genWindow->addShaderStreamedParameter(thegeom[3],2,GeneralWindows::ParameterType::PT_VEC3,Neumont::Vertex::STRIDE,Neumont::Vertex::NORMAL_OFFSET);
	genWindow->addShaderStreamedParameter(thegeom[3],3,GeneralWindows::ParameterType::PT_VEC2,Neumont::Vertex::STRIDE,Neumont::Vertex::UV_OFFSET);
	theRend[4]=genWindow->addRenderable(thegeom[3],theLightTransform,theShadeInfo[2],thetexture[1]);


	thetexture[2]= genWindow->addTexture("Shapes.png");
	theShadeInfo[3]=genWindow->createShaderInfo("LightShader.glsl","lightFragment.glsl");
	theRend[3]=genWindow->addRenderable(thegeom[0],planeNormTransform,theShadeInfo[3],thetexture[2]);
	theShadeInfo[5]=genWindow->createShaderInfo("noLightingVertexShader.glsl","noiseFragment.glsl");
	theRend[6]=genWindow->addRenderable(thegeom[1],cubeTransform,theShadeInfo[5],thetexture[1]);
	theShadeInfo[6]=genWindow->createShaderInfo("noiseVertexShader.glsl","noiseFragment.glsl");
	theRend[7]=genWindow->addRenderable(thegeom[1],cubeTransform,theShadeInfo[6],thetexture[1]);

	Neumont::ShapeData theTeapot=Neumont::ShapeGenerator::makeTeapot(16,mat4());
	thegeom[5]=genWindow->addGeometry(theTeapot.verts,theTeapot.vertexBufferSize(),theTeapot.indexBufferSize(),theTeapot.indices,theTeapot.numIndices,GL_TRIANGLES);
	theShadeInfo[7]= genWindow->createShaderInfo("noLightingVertexShader.glsl","MeltingFragShader.glsl");
	genWindow->addShaderStreamedParameter(thegeom[5],0,GeneralWindows::ParameterType::PT_VEC3,Neumont::Vertex::STRIDE,Neumont::Vertex::POSITION_OFFSET);
	genWindow->addShaderStreamedParameter(thegeom[5],1,GeneralWindows::ParameterType::PT_VEC4,Neumont::Vertex::STRIDE,Neumont::Vertex::COLOR_OFFSET);
	genWindow->addShaderStreamedParameter(thegeom[5],2,GeneralWindows::ParameterType::PT_VEC3,Neumont::Vertex::STRIDE,Neumont::Vertex::NORMAL_OFFSET);
	genWindow->addShaderStreamedParameter(thegeom[5],3,GeneralWindows::ParameterType::PT_VEC2,Neumont::Vertex::STRIDE,Neumont::Vertex::UV_OFFSET);
	theRend[8]=genWindow->addRenderable(thegeom[5],sphereTransform,theShadeInfo[7],thetexture[1]);
	
	theShadeInfo[8]= genWindow->createShaderInfo("noLightingVertexShader.glsl","noTextureFragmentShader.glsl");
	theShadeInfo[9]= genWindow->createShaderInfo("noLightingVertexShader.glsl","MultipleColorPassesFrag.glsl");
	theShadeInfo[10]= genWindow->createShaderInfo("noLightingVertexShader.glsl","depthFragShader.glsl");
	theRend[9]=genWindow->addRenderable(thegeom[5],sphereTransform,theShadeInfo[8],NULL);
	theRend[10]=genWindow->addRenderable(thegeom[1],sphereTransform,theShadeInfo[9],NULL);
	theRend[11]=genWindow->addRenderable(thegeom[1],sphereTransform,theShadeInfo[10],NULL);
	
	const char* meCubeMap[]={"Tantolunden6/posx.png","Tantolunden6/negx.png","Tantolunden6/posy.png","Tantolunden6/negy.png","Tantolunden6/posz.png","Tantolunden6/negz.png"};
	const char* meCubeMap2[]={"XYZCubeMap/posX.png","XYZCubeMap/negX.png","XYZCubeMap/posY.png","XYZCubeMap/negY.png","XYZCubeMap/posZ.png","XYZCubeMap/negZ.png"};
	thetexture[3]= genWindow->addCubeMap(meCubeMap);
	thetexture[4]= genWindow->addCubeMap(meCubeMap2);
	theShadeInfo[11]=genWindow->createShaderInfo("temp.glsl","CubeMapFragShade.glsl");
	theShadeInfo[12]=genWindow->createShaderInfo("SkyBoxHieghtVert.glsl","CubeMapFragShade.glsl");
	theRend[12]=genWindow->addRenderable(thegeom[3],theLightTransform,theShadeInfo[11],thetexture[3]);
	theRend[13]=genWindow->addRenderable(thegeom[5],theLightTransform,theShadeInfo[11],thetexture[3]);
	theRend[14]=genWindow->addRenderable(thegeom[2],theLightTransform,theShadeInfo[11],thetexture[3]);
	theRend[15]=genWindow->addRenderable(thegeom[1],theLightTransform,theShadeInfo[12],thetexture[3]);
	theRend[16]=genWindow->addRenderable(thegeom[1],theLightTransform,theShadeInfo[7],thetexture[3]);
	connect(&myTimer,SIGNAL(timeout()),this,SLOT(myUpdate()));
	myTimer.start(0); 
	
	inAlpha=0.51f;
	DebugMe.slideFloat("Alpha",inAlpha,0.0f,1.0f,"Alpha_Blending");
	visible=false;
	DebugMe.toggleBool("Draw Sowrd",visible,"first");
	Alpha_Blending=false;
	DebugMe.toggleBool("Draw Dragon",Alpha_Blending,"Alpha_Blending");
	DebugMe.slideFloat("X Axis",xAxis,-10.0f,10.0f,"Flat_Plane");
	DebugMe.slideFloat("Y Axis",yAxis,-10.0f,10.0f,"Flat_Plane");
	DebugMe.slideFloat("Z Axis",zAxis,-10.0f,10.0f,"Flat_Plane");
	x_Rotation=0;
	DebugMe.slideFloat("Rotate x Axis",x_Rotation,-180.0f,180.0f,"Flat_Plane");
	y_Rotation=0;
	DebugMe.slideFloat("Rotate y Axis",y_Rotation,-180.0f,180.0f,"Flat_Plane");
	spec=3.45f;

	DebugMe.slideFloat("Spec Power",spec,10.0f,50.0f,"Flat_Plane");
	show_plane=false;
	DebugMe.toggleBool("Draw Plane Normal",show_plane,"Flat_Plane");
	show_Cube=false;
	
	DebugMe.toggleBool("Draw Cube Normal",show_Cube,"Flat_Plane");
	noiseSlider=1.0f;
	theRend[4]->visible=false;
	DebugMe.toggleBool("Draw Light",theRend[4]->visible,"Flat_Plane");
	DebugMe.slideFloat("Octave",noiseSlider,0.0f,3.0f,"Noise_Texture");
	theRend[6]->visible=false;
	DebugMe.toggleBool("Draw Noise Plane",theRend[6]->visible,"Noise_Texture");
	theRend[7]->visible=false;
	DebugMe.toggleBool("Draw Height Map",theRend[7]->visible,"Noise_Texture");

	threshHold=0.0f;
	DebugMe.slideFloat("ThreshHold",threshHold,0.0f,1.0f,"Melting_Teapot");
	theRend[8]->visible=false;
	DebugMe.toggleBool("Draw Melting TeaPot",theRend[8]->visible,"Melting_Teapot");

	theRend[9]->visible=false;
	DebugMe.toggleBool("Draw TeaPot",theRend[9]->visible,"Multiple_Passes");
	theRend[10]->visible=false;
	DebugMe.toggleBool("Draw Color Plane",theRend[10]->visible,"Multiple_Passes");
	theRend[11]->visible=false;
	DebugMe.toggleBool("Draw Depth Plane",theRend[11]->visible,"Multiple_Passes");

	theRend[12]->visible=true;
	DebugMe.toggleBool("Draw The Cube",theRend[12]->visible,"Environment_Map");

	theRend[13]->visible=true;
	DebugMe.toggleBool("Draw The teaPot or Sphere",theRend[13]->visible,"Environment_Map");
	theRend[15]->visible=false;
	MapOneOrTwo=0.0f;
	DebugMe.slideFloat("Selected Map",MapOneOrTwo,0.0f,2.0f,"Environment_Map");
	makeCube();
	makeNoiseTex();
	
}

void BinaryAlphaScene::myUpdate()
{
	thebug.DebugShapeUpadte();
	DebugMe.updateValues();
	//w

	
	if(GetAsyncKeyState(87))
		theCamera.moveFoward();
		
	if(GetAsyncKeyState(83))
		theCamera.movBackward();
	
	if(GetAsyncKeyState(65))
		theCamera.strafeLeft();
	
	if(GetAsyncKeyState(68))
		theCamera.strafeRight();
	
	if(GetAsyncKeyState(82))
		theCamera.moveUp();
	
	if(GetAsyncKeyState(70))
		theCamera.moveDown();
		//glViewport(0,0,width(),height());
	repaint();
	//qDebug()<<theCamera.position.x;


}
void BinaryAlphaScene::paintGL()
{
	mat4 shadowMatrix= mat4( 0.5f,0,0,0.5f,
							 0,0.5f,0,0.5f,
							 0,0,0.5f,0.5f,
							 0,0,0,1.0f);
	
	glBindFramebuffer(GL_FRAMEBUFFER,myFrameBuffer);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glViewport(0,0,width(),height());
		drawStuff();
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glViewport(0,0,width(),height());
	
	drawStuff();
	  mat4 shadePlaneTranslation=glm::translate(mat4(),glm::vec3(0.2f,0.3f,-1.0f));
	 mat4 shadePlaneRotationY=glm::rotate(mat4(),90.0f,glm::vec3(1.0f,0.0f,0.0f));
	 mat4 shadePlaneRotationX=glm::rotate(mat4(),0.0f,glm::vec3(0.0f,1.0f,0.0f));
	 mat4 shadePlaneScale=glm::scale(mat4(),vec3(.5f,0.5f,1.0f));
	 mat4 shadePlaneTransform= shadePlaneTranslation*(shadePlaneRotationX*shadePlaneRotationY)*shadePlaneScale;
	float teaAlpha=1;

glBindTexture(GL_TEXTURE_2D,myColorTextureID);
	   genWindow->addRenderableUniformParameter(theRend[10],"transform",GeneralWindows::ParameterType::PT_MAT4,&shadePlaneTransform[0][0]);
	   genWindow->addRenderableUniformParameter(theRend[10],"Alpha",GeneralWindows::ParameterType::PT_FLOAT,&teaAlpha);
	  glBindVertexArray(thegeom[1]->geomitryID);
	  if(theRend[10]->visible)
	  {
	  glDrawElements(
	 	 GL_TRIANGLES,
	 	 thegeom[1]->geomIndicies,
	 	 GL_UNSIGNED_SHORT,
	 	 (void*)(thegeom[1]->geometryOffset));
	   }

	   mat4 shadeDepthPlaneTranslation=glm::translate(mat4(),glm::vec3(0.6f,0.3f,-1.0f));
	 mat4 shadeDepthPlaneRotationY=glm::rotate(mat4(),90.0f,glm::vec3(1.0f,0.0f,0.0f));
	 mat4 shadeDepthPlaneRotationX=glm::rotate(mat4(),0.0f,glm::vec3(0.0f,1.0f,0.0f));
	 mat4 shadeDepthPlaneScale=glm::scale(mat4(),vec3(.5f,0.5f,1.0f));
	 mat4 shadeDepthPlaneTransform= shadeDepthPlaneTranslation*(shadeDepthPlaneRotationX*shadeDepthPlaneRotationY)*shadeDepthPlaneScale;
	

glBindTexture(GL_TEXTURE_2D,myDepthTextureID);
	   genWindow->addRenderableUniformParameter(theRend[11],"transform",GeneralWindows::ParameterType::PT_MAT4,&shadeDepthPlaneTransform[0][0]);
	   genWindow->addRenderableUniformParameter(theRend[11],"Alpha",GeneralWindows::ParameterType::PT_FLOAT,&teaAlpha);
	  glBindVertexArray(thegeom[1]->geomitryID);
	  if(theRend[11]->visible)
	  {
	  glDrawElements(
	 	 GL_TRIANGLES,
	 	 thegeom[1]->geomIndicies,
	 	 GL_UNSIGNED_SHORT,
	 	 (void*)(thegeom[1]->geometryOffset));
	   }
	//glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 1);
}
void BinaryAlphaScene::drawStuff()
{
	mat4 projection=glm::perspective(60.0f,(float)width()/(float)height(),0.1f,70.0f);
	 theRend[0]->visible=visible;
	
	 
	
	  mat4 theLightTranslation=glm::translate(mat4(),glm::vec3(xAxis,yAxis,zAxis));
	 
	theLightTransform=projection*theCamera.getWorldToViewMatrix()*theLightTranslation;

	   genWindow->addRenderableUniformParameter(theRend[4],"transform",GeneralWindows::ParameterType::PT_MAT4,&theLightTransform[0][0]);
	   genWindow->addRenderableUniformParameter(theRend[4],"Alpha",GeneralWindows::ParameterType::PT_FLOAT,&inAlpha);
	  glBindVertexArray(thegeom[3]->geomitryID);
	 if(theRend[4]->visible)
	 {
		  glDrawElements(
	 		 GL_TRIANGLES,
	 		 thegeom[3]->geomIndicies,
	 		 GL_UNSIGNED_SHORT,
	 		 (void*)(thegeom[3]->geometryOffset));
	 }
	   mat4 planeTranslation=glm::translate(mat4(),glm::vec3(1.0f,-2.0f,-6.0f));
	 mat4 planeRotationY=glm::rotate(mat4(),20.0f,glm::vec3(1.0f,0.0f,0.0f));
	 mat4 planeRotationX=glm::rotate(mat4(),0.0f,glm::vec3(0.0f,1.0f,0.0f));
	 mat4 scalePlane=glm::scale(mat4(),glm::vec3(2.0f,2.0f,2.0f));
	planeTransform=projection*theCamera.getWorldToViewMatrix()*planeTranslation*planeRotationX*planeRotationY*scalePlane;
	 
	   genWindow->addRenderableUniformParameter(theRend[0],"transform",GeneralWindows::ParameterType::PT_MAT4,&planeTransform[0][0]);
	   glBindTexture(GL_TEXTURE_2D,thetexture[0]->textureID);
	  glBindVertexArray(thegeom[1]->geomitryID);
	  if(theRend[0]->visible)
	  {
	  glDrawElements(
	 	 GL_TRIANGLES,
	 	 thegeom[1]->geomIndicies,
	 	 GL_UNSIGNED_SHORT,
	 	 (void*)(thegeom[1]->geometryOffset));
	  }
	
	 	 
	 mat4 cubeTranslation=glm::translate(mat4(),glm::vec3(5.0f,-2.0f,-17.0f));
	 mat4 cubeRotationY=glm::rotate(mat4(),20.0f,glm::vec3(1.0f,0.0f,0.0f));
	 mat4 cubeRotationX=glm::rotate(mat4(),0.0f,glm::vec3(0.0f,1.0f,0.0f));
	 //mat4 scalePlane=glm::scale(mat4(),glm::vec3(2.0f,2.0f,2.0f));
	cubeTransform=projection*theCamera.getWorldToViewMatrix()*cubeTranslation*cubeRotationX*cubeRotationY;
	glBindTexture(GL_TEXTURE_2D,thetexture[1]->textureID);
	   genWindow->addRenderableUniformParameter(theRend[1],"transform",GeneralWindows::ParameterType::PT_MAT4,&cubeTransform[0][0]);
	  glBindVertexArray(thegeom[1]->geomitryID);
	  if(Alpha_Blending)
	  {
	  glDrawElements(
	 	 GL_TRIANGLES,
	 	 thegeom[1]->geomIndicies,
	 	 GL_UNSIGNED_SHORT,
	 	 (void*)(thegeom[1]->geometryOffset));
	  }

	   mat4 sphereTranslation=glm::translate(mat4(),glm::vec3(5.0f,-4.0f,-20.0f));
	 mat4 sphereRotationY=glm::rotate(mat4(),20.0f,glm::vec3(1.0f,0.0f,0.0f));
	 mat4 sphereRotationX=glm::rotate(mat4(),0.0f,glm::vec3(0.0f,1.0f,0.0f));
	 mat4 scalesphere=glm::scale(mat4(),glm::vec3(30.0f,40.0f,30.0f));
	sphereTransform=projection*theCamera.getWorldToViewMatrix()*sphereTranslation*sphereRotationX*sphereRotationY*scalesphere;

	   genWindow->addRenderableUniformParameter(theRend[2],"transform",GeneralWindows::ParameterType::PT_MAT4,&sphereTransform[0][0]);
	   genWindow->addRenderableUniformParameter(theRend[2],"Alpha",GeneralWindows::ParameterType::PT_FLOAT,&inAlpha);
	  glBindVertexArray(thegeom[2]->geomitryID);
	   if(Alpha_Blending)
	  {
	  glDrawElements(
	 	 GL_TRIANGLES,
	 	 thegeom[2]->geomIndicies,
	 	 GL_UNSIGNED_SHORT,
	 	 (void*)(thegeom[2]->geometryOffset));
	   }
 

	    mat4 planeNormTranslation=glm::translate(mat4(),glm::vec3(1.0f,-2.0f,-3.0f));
	 mat4 planeNormRotationY=glm::rotate(mat4(),270.0f,glm::vec3(1.0f,0.0f,0.0f));
	 mat4 planeNormRotationX=glm::rotate(mat4(),0.0f,glm::vec3(0.0f,1.0f,0.0f));
	 mat4 ModelToWorld=planeNormTranslation*planeNormRotationX*planeNormRotationY;
	 planeNormTransform=projection*theCamera.getWorldToViewMatrix()*planeNormTranslation*planeNormRotationX*planeNormRotationY;
	 glBindTexture(GL_TEXTURE_2D,thetexture[2]->textureID);
		 genWindow->addRenderableUniformParameter(theRend[3],"transform",GeneralWindows::ParameterType::PT_MAT4,&planeNormTransform[0][0]);
		 genWindow->addRenderableUniformParameter(theRend[3],"ambientLight",GeneralWindows::ParameterType::PT_VEC3,&glm::vec3(1.0f,1.0f,1.0f)[0]);
		 genWindow->addRenderableUniformParameter(theRend[3],"lightPosition",GeneralWindows::ParameterType::PT_VEC3,&glm::vec3(xAxis,yAxis,zAxis)[0]);
		 genWindow->addRenderableUniformParameter(theRend[3],"power",GeneralWindows::ParameterType::PT_FLOAT,&spec);
		 genWindow->addRenderableUniformParameter(theRend[3],"specCol",GeneralWindows::ParameterType::PT_VEC3,&glm::vec3(1.0f,1.0f,1.0f)[0]);
		 genWindow->addRenderableUniformParameter(theRend[3],"eyePosition",GeneralWindows::ParameterType::PT_VEC3,&theCamera.position[0]);
		 genWindow->addRenderableUniformParameter(theRend[3],"modelToWorld",GeneralWindows::ParameterType::PT_MAT4,&ModelToWorld[0][0]);
		 genWindow->addRenderableUniformParameter(theRend[3],"lightColor",GeneralWindows::ParameterType::PT_VEC3,&glm::vec3(1.0f,1.0f,1.0f)[0]);

		  glBindVertexArray(thegeom[0]->geomitryID);
	  if(show_plane)
	  {
		  glDrawElements(
	 		 GL_TRIANGLES,
	 		 thegeom[0]->geomIndicies,
	 		 GL_UNSIGNED_SHORT,
	 		 (void*)(thegeom[0]->geometryOffset));
	  }

	  
	    mat4 cubeNormTranslation=glm::translate(mat4(),glm::vec3(1.0f,-2.0f,-3.0f));
	 mat4 cubeNormRotationY=glm::rotate(mat4(),y_Rotation,glm::vec3(1.0f,0.0f,0.0f));
	 mat4 cubeNormRotationX=glm::rotate(mat4(),x_Rotation,glm::vec3(0.0f,1.0f,0.0f));
	 mat4 cubeNormalScale=glm::scale(mat4(),vec3(3.0f,3.0f,3.0f));
	 ModelToWorld=cubeNormTranslation*cubeNormRotationX*cubeNormRotationY;
	 customCubeTransform=projection*theCamera.getWorldToViewMatrix()*cubeNormTranslation*cubeNormRotationX*cubeNormRotationY;

	 glBindTexture(GL_TEXTURE_2D,thetexture[2]->textureID);
	 genWindow->addRenderableUniformParameter(theRend[5],"modelToWorld",GeneralWindows::ParameterType::PT_MAT4,&ModelToWorld[0][0]);
	 genWindow->addRenderableUniformParameter(theRend[5],"transform",GeneralWindows::ParameterType::PT_MAT4,&customCubeTransform[0][0]);
	 genWindow->addRenderableUniformParameter(theRend[5],"ambientLight",GeneralWindows::ParameterType::PT_VEC3,&glm::vec3(1.0f,1.0f,1.0f)[0]);
	 genWindow->addRenderableUniformParameter(theRend[5],"lightPosition",GeneralWindows::ParameterType::PT_VEC3,&glm::vec3(xAxis,yAxis,zAxis)[0]);
	 genWindow->addRenderableUniformParameter(theRend[5],"power",GeneralWindows::ParameterType::PT_FLOAT,&spec);
	 genWindow->addRenderableUniformParameter(theRend[5],"specCol",GeneralWindows::ParameterType::PT_VEC3,&glm::vec3(1.0f,1.0f,1.0f)[0]);
	 genWindow->addRenderableUniformParameter(theRend[5],"eyePosition",GeneralWindows::ParameterType::PT_VEC3,&theCamera.position[0]);
	 genWindow->addRenderableUniformParameter(theRend[5],"lightColor",GeneralWindows::ParameterType::PT_VEC3,&glm::vec3(1.0f,1.0f,1.0f)[0]);
	   
	    glBindVertexArray(thegeom[4]->geomitryID);
	  if(show_Cube)
	  {
		  glDrawElements(
	 		 GL_TRIANGLES,
	 		 thegeom[4]->geomIndicies,
	 		 GL_UNSIGNED_SHORT,
	 		 (void*)(thegeom[4]->geometryOffset));
	  }
	 cubeTranslation=glm::translate(mat4(),glm::vec3(5.0f,-2.0f,-17.0f));
	 cubeRotationY=glm::rotate(mat4(),20.0f,glm::vec3(1.0f,0.0f,0.0f));
	 cubeRotationX=glm::rotate(mat4(),0.0f,glm::vec3(0.0f,1.0f,0.0f));
	 //mat4 scalePlane=glm::scale(mat4(),glm::vec3(2.0f,2.0f,2.0f));
	cubeTransform=projection*theCamera.getWorldToViewMatrix()*cubeTranslation*cubeRotationX*cubeRotationY;
	glBindTexture(GL_TEXTURE_2D,texID);
	
		genWindow->addRenderableUniformParameter(theRend[6],"Octave",GeneralWindows::ParameterType::PT_FLOAT,&noiseSlider);
	  genWindow->addRenderableUniformParameter(theRend[6],"transform",GeneralWindows::ParameterType::PT_MAT4,&cubeTransform[0][0]);
	  glBindVertexArray(thegeom[1]->geomitryID);
	  if(theRend[6]->visible)
	  {
		  glDrawElements(
	 		 GL_TRIANGLES,
	 		 thegeom[1]->geomIndicies,
	 		 GL_UNSIGNED_SHORT,
	 		 (void*)(thegeom[1]->geometryOffset));
	  }
	   cubeTranslation=glm::translate(mat4(),glm::vec3(5.0f,-1.0f,-14.0f));
	 cubeRotationY=glm::rotate(mat4(),20.0f,glm::vec3(1.0f,0.0f,0.0f));
	 cubeRotationX=glm::rotate(mat4(),0.0f,glm::vec3(0.0f,1.0f,0.0f));
	 //mat4 scalePlane=glm::scale(mat4(),glm::vec3(2.0f,2.0f,2.0f));
	cubeTransform=projection*theCamera.getWorldToViewMatrix()*cubeTranslation*cubeRotationX*cubeRotationY;
	glBindTexture(GL_TEXTURE_2D,texID);
	
		genWindow->addRenderableUniformParameter(theRend[7],"Octave",GeneralWindows::ParameterType::PT_FLOAT,&noiseSlider);
	   genWindow->addRenderableUniformParameter(theRend[7],"transform",GeneralWindows::ParameterType::PT_MAT4,&cubeTransform[0][0]);
	
	   glBindVertexArray(thegeom[1]->geomitryID);
	 if(theRend[7]->visible)
	  {
		  glDrawElements(
	 		 GL_TRIANGLES,
	 		 thegeom[1]->geomIndicies,
	 		 GL_UNSIGNED_SHORT,
	 		 (void*)(thegeom[1]->geometryOffset));
	 }

	   cubeTranslation=glm::translate(mat4(),glm::vec3(5.0f,4.0f,+10.0f));
	 cubeRotationY=glm::rotate(mat4(),-90.0f,glm::vec3(1.0f,0.0f,0.0f));
	 cubeRotationX=glm::rotate(mat4(),0.0f,glm::vec3(0.0f,1.0f,0.0f));
	 //mat4 scalePlane=glm::scale(mat4(),glm::vec3(2.0f,2.0f,2.0f));
	cubeTransform=projection*theCamera.getWorldToViewMatrix()*cubeTranslation*cubeRotationX*cubeRotationY;
	glBindTexture(GL_TEXTURE_2D,texID);
	
		genWindow->addRenderableUniformParameter(theRend[8],"Octave",GeneralWindows::ParameterType::PT_FLOAT,&noiseSlider);
	   genWindow->addRenderableUniformParameter(theRend[8],"transform",GeneralWindows::ParameterType::PT_MAT4,&cubeTransform[0][0]);
	  genWindow->addRenderableUniformParameter(theRend[8],"threshHold",GeneralWindows::ParameterType::PT_FLOAT,&threshHold);
	   glBindVertexArray(thegeom[5]->geomitryID);
	 if(theRend[8]->visible)
	  {
		  glDrawElements(
	 		 GL_TRIANGLES,
	 		 thegeom[5]->geomIndicies,
	 		 GL_UNSIGNED_SHORT,
	 		 (void*)(thegeom[5]->geometryOffset));
	 }
	  
	 float teaAlpha=1;
	  mat4 shadeTeapotTranslation=glm::translate(mat4(),glm::vec3(0.0f,0.0f,0.0f));
	 mat4 shadeTeapotRotationY=glm::rotate(mat4(),20.0f,glm::vec3(1.0f,0.0f,0.0f));
	 mat4 shadeTeapotRotationX=glm::rotate(mat4(),0.0f,glm::vec3(0.0f,1.0f,0.0f));
	mat4 shadeTeapotTransform=projection*theCamera.getWorldToViewMatrix()*shadeTeapotTranslation*shadeTeapotRotationX*shadeTeapotRotationY;
	
	   genWindow->addRenderableUniformParameter(theRend[9],"transform",GeneralWindows::ParameterType::PT_MAT4,&shadeTeapotTransform[0][0]);
	   genWindow->addRenderableUniformParameter(theRend[9],"Alpha",GeneralWindows::ParameterType::PT_FLOAT,&teaAlpha);
	  glBindVertexArray(thegeom[5]->geomitryID);
	  if(theRend[9]->visible)
	  {
	  glDrawElements(
	 	 GL_TRIANGLES,
	 	 thegeom[5]->geomIndicies,

	 	 GL_UNSIGNED_SHORT,
	 	 (void*)(thegeom[5]->geometryOffset));
	   }


	   mat4 skyBoxTrans=glm::translate(mat4(),glm::vec3(3.0f,3.0f,17.0f));
	 mat4 skyBoxRotationY=glm::rotate(mat4(),0.0f,glm::vec3(1.0f,0.0f,0.0f));
	 mat4 skyBoxRotationX=glm::rotate(mat4(),0.0f,glm::vec3(0.0f,1.0f,0.0f));
	  mat4 scaleSkyBox=glm::scale(mat4(),glm::vec3(10.0f,10.0f,10.0f));
	 mat4 skyBoxModelView= theCamera.getWorldToViewMatrix()*skyBoxTrans*skyBoxRotationX*skyBoxRotationY*scaleSkyBox;
	mat4 skyBoxTransform=projection*theCamera.getWorldToViewMatrix()*skyBoxTrans*skyBoxRotationX*skyBoxRotationY*scaleSkyBox;
	mat4 skyBoxModel=skyBoxTrans*skyBoxRotationX*skyBoxRotationY*scaleSkyBox;
	float isSkyBox=1;
	   genWindow->addRenderableUniformParameter(theRend[12],"transform",GeneralWindows::ParameterType::PT_MAT4,&skyBoxTransform[0][0]);
	   genWindow->addRenderableUniformParameter(theRend[12],"DrawSkyBox",GeneralWindows::ParameterType::PT_FLOAT,&isSkyBox);
	   genWindow->addRenderableUniformParameter(theRend[12],"WorldCameraPosition",GeneralWindows::ParameterType::PT_VEC3,&theCamera.position[0]);
	   genWindow->addRenderableUniformParameter(theRend[12],"ModelViewMatrix",GeneralWindows::ParameterType::PT_MAT4,&skyBoxModelView[0][0]);
	   genWindow->addRenderableUniformParameter(theRend[12],"ModelMatrix",GeneralWindows::ParameterType::PT_MAT4,&skyBoxModel[0][0]);
	   genWindow->addRenderableUniformParameter(theRend[12],"ProjectionMatrix",GeneralWindows::ParameterType::PT_MAT4,&projection[0][0]);
	   glm::vec4 matColor= glm::vec4(0.0f,0.0f,0.0f,1.0f);
	   genWindow->addRenderableUniformParameter(theRend[12],"MaterialColor",GeneralWindows::ParameterType::PT_VEC4,&matColor[0]);
	   float reflecFactor=1.0f;
	   genWindow->addRenderableUniformParameter(theRend[12],"ReflectFactor",GeneralWindows::ParameterType::PT_FLOAT,&reflecFactor);
	    if(MapOneOrTwo<=1.0f)
	   {
		glBindTexture(GL_TEXTURE_CUBE_MAP,thetexture[3]->textureID);
	   }
	   else 
	   {
		   glBindTexture(GL_TEXTURE_CUBE_MAP,thetexture[4]->textureID);
	   }
	  glBindVertexArray(thegeom[3]->geomitryID);
	  if(theRend[12]->visible)
	  {
	  glDrawElements(
	 	 GL_TRIANGLES,
	 	 thegeom[3]->geomIndicies,

	 	 GL_UNSIGNED_SHORT,
	 	 (void*)(thegeom[3]->geometryOffset));
	   }

	  
	   mat4 reflecTrans=glm::translate(mat4(),glm::vec3(3.0f,3.0f,17.0f));
	 mat4 reflecRotationY=glm::rotate(mat4(),-90.0f,glm::vec3(1.0f,0.0f,0.0f));
	 mat4 reflecRotationX=glm::rotate(mat4(),0.0f,glm::vec3(0.0f,1.0f,0.0f));
	 mat4 reflecModelView= theCamera.getWorldToViewMatrix()*reflecTrans*reflecRotationX*reflecRotationY;
	mat4 reflecTransform=projection*theCamera.getWorldToViewMatrix()*reflecTrans*reflecRotationX*reflecRotationY;
	mat4 reflecModel=reflecTrans*reflecRotationX*reflecRotationY;
	isSkyBox=0;
	   genWindow->addRenderableUniformParameter(theRend[13],"transform",GeneralWindows::ParameterType::PT_MAT4,&reflecTransform[0][0]);
	   genWindow->addRenderableUniformParameter(theRend[13],"DrawSkyBox",GeneralWindows::ParameterType::PT_FLOAT,&isSkyBox);
	   genWindow->addRenderableUniformParameter(theRend[13],"WorldCameraPosition",GeneralWindows::ParameterType::PT_VEC3,&theCamera.position[0]);
	   genWindow->addRenderableUniformParameter(theRend[13],"ModelViewMatrix",GeneralWindows::ParameterType::PT_MAT4,&reflecModelView[0][0]);
	   genWindow->addRenderableUniformParameter(theRend[13],"ModelMatrix",GeneralWindows::ParameterType::PT_MAT4,&reflecModel[0][0]);
	   genWindow->addRenderableUniformParameter(theRend[13],"ProjectionMatrix",GeneralWindows::ParameterType::PT_MAT4,&projection[0][0]);
	   
	   genWindow->addRenderableUniformParameter(theRend[13],"MaterialColor",GeneralWindows::ParameterType::PT_VEC4,&matColor[0]);
	   reflecFactor=1.0f;
	   genWindow->addRenderableUniformParameter(theRend[13],"ReflectFactor",GeneralWindows::ParameterType::PT_FLOAT,&reflecFactor);
	   if(MapOneOrTwo<=1.0f)
	   {
		glBindTexture(GL_TEXTURE_CUBE_MAP,thetexture[3]->textureID);
	   }
	   else 
	   {
		   glBindTexture(GL_TEXTURE_CUBE_MAP,thetexture[4]->textureID);
	   }
	  glBindVertexArray(thegeom[5]->geomitryID);
	  if(theRend[13]->visible)
	  {
	  glDrawElements(
	 	 GL_TRIANGLES,
	 	 thegeom[5]->geomIndicies,

	 	 GL_UNSIGNED_SHORT,
	 	 (void*)(thegeom[5]->geometryOffset));
	   }

	   reflecTrans=glm::translate(mat4(),glm::vec3(3.0f,3.0f,17.0f));
	   reflecRotationY=glm::rotate(mat4(),0.0f,glm::vec3(1.0f,0.0f,0.0f));
	   reflecRotationX=glm::rotate(mat4(),0.0f,glm::vec3(0.0f,1.0f,0.0f));
	   reflecModelView= theCamera.getWorldToViewMatrix()*reflecTrans*reflecRotationX*reflecRotationY;
	   reflecTransform=projection*theCamera.getWorldToViewMatrix()*reflecTrans*reflecRotationX*reflecRotationY;
	   reflecModel=reflecTrans*reflecRotationX*reflecRotationY;
	   genWindow->addRenderableUniformParameter(theRend[14],"transform",GeneralWindows::ParameterType::PT_MAT4,&reflecTransform[0][0]);
	   genWindow->addRenderableUniformParameter(theRend[14],"DrawSkyBox",GeneralWindows::ParameterType::PT_FLOAT,&isSkyBox);
	   genWindow->addRenderableUniformParameter(theRend[14],"WorldCameraPosition",GeneralWindows::ParameterType::PT_VEC3,&theCamera.position[0]);
	   genWindow->addRenderableUniformParameter(theRend[14],"ModelViewMatrix",GeneralWindows::ParameterType::PT_MAT4,&reflecModelView[0][0]);
	   genWindow->addRenderableUniformParameter(theRend[14],"ModelMatrix",GeneralWindows::ParameterType::PT_MAT4,&reflecModel[0][0]);
	   genWindow->addRenderableUniformParameter(theRend[14],"ProjectionMatrix",GeneralWindows::ParameterType::PT_MAT4,&projection[0][0]);
	   
	   genWindow->addRenderableUniformParameter(theRend[14],"MaterialColor",GeneralWindows::ParameterType::PT_VEC4,&matColor[0]);
	   reflecFactor=1.0f;
	   genWindow->addRenderableUniformParameter(theRend[14],"ReflectFactor",GeneralWindows::ParameterType::PT_FLOAT,&reflecFactor);
	   if(MapOneOrTwo<=1.0f)
	   {
		glBindTexture(GL_TEXTURE_CUBE_MAP,thetexture[3]->textureID);
	   }
	   else 
	   {
		   glBindTexture(GL_TEXTURE_CUBE_MAP,thetexture[4]->textureID);
	   }
	    glBindVertexArray(thegeom[2]->geomitryID);
	  if(!theRend[13]->visible)
	  {
	  glDrawElements(
	 	 GL_TRIANGLES,
	 	 thegeom[2]->geomIndicies,

	 	 GL_UNSIGNED_SHORT,
	 	 (void*)(thegeom[2]->geometryOffset));
	   }


	   genWindow->addRenderableUniformParameter(theRend[15],"transform",GeneralWindows::ParameterType::PT_MAT4,&reflecTransform[0][0]);
	   genWindow->addRenderableUniformParameter(theRend[15],"DrawSkyBox",GeneralWindows::ParameterType::PT_FLOAT,&isSkyBox);
	   genWindow->addRenderableUniformParameter(theRend[15],"WorldCameraPosition",GeneralWindows::ParameterType::PT_VEC3,&theCamera.position[0]);
	   genWindow->addRenderableUniformParameter(theRend[15],"ModelViewMatrix",GeneralWindows::ParameterType::PT_MAT4,&reflecModelView[0][0]);
	   genWindow->addRenderableUniformParameter(theRend[15],"ModelMatrix",GeneralWindows::ParameterType::PT_MAT4,&reflecModel[0][0]);
	   genWindow->addRenderableUniformParameter(theRend[15],"ProjectionMatrix",GeneralWindows::ParameterType::PT_MAT4,&projection[0][0]);
	   genWindow->addRenderableUniformParameter(theRend[15],"Octave",GeneralWindows::ParameterType::PT_FLOAT,&noiseSlider);
	   genWindow->addRenderableUniformParameter(theRend[15],"MaterialColor",GeneralWindows::ParameterType::PT_VEC4,&matColor[0]);
	   reflecFactor=1.0f;
	   genWindow->addRenderableUniformParameter(theRend[15],"ReflectFactor",GeneralWindows::ParameterType::PT_FLOAT,&reflecFactor);
	  
		glBindTexture(GL_TEXTURE_CUBE_MAP,thetexture[3]->textureID);
		//glBindTexture(GL_TEXTURE_2D,texID);
	   
	 
	    glBindVertexArray(thegeom[1]->geomitryID);
	  if(theRend[15]->visible)
	  {
	  glDrawElements(
	 	 GL_TRIANGLES,
	 	 thegeom[1]->geomIndicies,

	 	 GL_UNSIGNED_SHORT,
	 	 (void*)(thegeom[1]->geometryOffset));
	   }
		
}
 void BinaryAlphaScene::mouseMoveEvent(QMouseEvent* e)
{
	if(e->buttons() && Qt::LeftButton)
	{
		theCamera.mouseUpdate(glm::vec2(e->x(),e->y()));
	}
	
}
void BinaryAlphaScene::keyPressEvent(QKeyEvent*e)
{
	
	if(GetAsyncKeyState(87))
		theCamera.moveFoward();
		
	if(GetAsyncKeyState(83))
		theCamera.movBackward();
	
	if(GetAsyncKeyState(65))
		theCamera.strafeLeft();
	
	if(GetAsyncKeyState(68))
		theCamera.strafeRight();
	
	if(GetAsyncKeyState(82))
		theCamera.moveUp();
	
	if(GetAsyncKeyState(70))
		theCamera.moveDown();
	
	
}
void BinaryAlphaScene::makeCube()
{
using glm::vec2;
using glm::vec3;
using glm::vec4;
Vertex stackVerts[] = 
{
// Top
vec3(-1.0f, +1.0f, +1.0f), // 0
vec3(+1.0f, +0.0f, +0.0f), // Tangent
vec3(+0.0f, +1.0f, +0.0f), // Normal
vec2(+0.0f, +1.0f), // UV
vec3(+1.0f, +1.0f, +1.0f), // 1
vec3(+1.0f, +0.0f, +0.0f), // Tangent
vec3(+1.0f, +0.0f, +0.0f), // Normal
vec2(+1.0f, +1.0f), // UV
vec3(+1.0f, +1.0f, -1.0f), // 2
vec3(+1.0f, +0.0f, +0.0f), // Tangent
vec3(+0.0f, +1.0f, +0.0f), // Normal
vec2(+1.0f, +0.0f), // UV
vec3(-1.0f, +1.0f, -1.0f), // 3
vec3(+1.0f, +0.0f, +0.0f), // Tangent
vec3(+0.0f, +1.0f, +0.0f), // Normal
vec2(+0.0f, +0.0f), // UV

// Front
//+X
vec3(-1.0f, +1.0f, -1.0f), // 4
vec3(+1.0f, +0.0f, +0.0f), // Tangent
vec3(+0.0f, +0.0f, -1.0f), // Normal
vec2(+0.0f, +1.0f), // UV
vec3(+1.0f, +1.0f, -1.0f), // 5
vec3(+1.0f, +0.0f, +0.0f), // Tangent
vec3(+0.0f, +0.0f, -1.0f), // Normal
vec2(+1.0f, +1.0f), // UV
vec3(+1.0f, -1.0f, -1.0f), // 6
vec3(+1.0f, +0.0f, +0.0f), // Tangent
vec3(+0.0f, +0.0f, -1.0f), // Normal
vec2(+1.0f, +0.0f), // UV
vec3(-1.0f, -1.0f, -1.0f), // 7
vec3(+1.0f, +0.0f, +0.0f), // Tangent
vec3(+0.0f, +0.0f, -1.0f), // Normal
vec2(+0.0f, +0.0f), // UV

// Right
//-z
vec3(+1.0f, +1.0f, -1.0f), // 8
vec3(+0.0f, +0.0f, -1.0f), // Tangent
vec3(+1.0f, +0.0f, +0.0f), // Normal
vec2(+1.0f, +0.0f), // UV
vec3(+1.0f, +1.0f, +1.0f), // 9
vec3(+0.0f, +0.0f, -1.0f), // Tangent
vec3(+1.0f, +0.0f, +0.0f), // Normal
vec2(+0.0f, +0.0f), // UV
vec3(+1.0f, -1.0f, +1.0f), // 10
vec3(+0.0f, +0.0f, -1.0f), // Tangent
vec3(+1.0f, +0.0f, +0.0f), // Normal
vec2(+0.0f, +1.0f), // UV
vec3(+1.0f, -1.0f, -1.0f), // 11
vec3(+0.0f, +0.0f, -1.0f), // Tangent
vec3(+1.0f, +0.0f, +0.0f), // Normal
vec2(+1.0f, +1.0f), // UV

// Left
//+z
vec3(-1.0f, +1.0f, +1.0f), // 12
vec3(+0.0f, +0.0f, +1.0f), // Tangent
vec3(-1.0f, +0.0f, +0.0f), // Normal
vec2(+1.0f, +0.0f), // UV
vec3(-1.0f, +1.0f, -1.0f), // 13
vec3(+0.0f, +0.0f, +1.0f), // Tangent
vec3(-1.0f, +0.0f, +0.0f), // Normal
vec2(+0.0f, +0.0f), // UV
vec3(-1.0f, -1.0f, -1.0f), // 14
vec3(+0.0f, +0.0f, +1.0f), // Tangent
vec3(-1.0f, +0.0f, +0.0f), // Normal
vec2(+0.0f, +1.0f), // UV
vec3(-1.0f, -1.0f, +1.0f), // 15
vec3(+0.0f, +0.0f, +1.0f), // Tangent
vec3(-1.0f, +0.0f, +0.0f), // Normal
vec2(+1.0f, +1.0f), // UV

// Back
//+x
vec3(+1.0f, +1.0f, +1.0f), // 16
vec3(+1.0f, +0.0f, +0.0f), // Tangent
vec3(+0.0f, +0.0f, +1.0f), // Normal
vec2(+1.0f, +0.0f), // UV
vec3(-1.0f, +1.0f, +1.0f), // 17
vec3(+1.0f, +0.0f, +0.0f), // Tangent
vec3(+0.0f, +0.0f, +1.0f), // Normal
vec2(+0.0f, +0.0f), // UV
vec3(-1.0f, -1.0f, +1.0f), // 18
vec3(+1.0f, +0.0f, +0.0f), // Tangent
vec3(+0.0f, +0.0f, +1.0f), // Normal
vec2(+0.0f, +1.0f), // UV
vec3(+1.0f, -1.0f, +1.0f), // 19
vec3(+1.0f, +0.0f, +0.0f), // Tangent
vec3(+0.0f, +0.0f, +1.0f), // Normal
vec2(+1.0f, +1.0f), // UV

// Bottom
//+x
vec3(+1.0f, -1.0f, -1.0f), // 20
vec3(+1.0f, +0.0f, +0.0f), // Tangent
vec3(+0.0f, -1.0f, +0.0f), // Normal
vec2(+1.0f, +1.0f), // UV
vec3(-1.0f, -1.0f, -1.0f), // 21
vec3(+1.0f, +0.0f, +0.0f), // Tangent
vec3(+0.0f, -1.0f, +0.0f), // Normal
vec2(+0.0f, +1.0f), // UV
vec3(-1.0f, -1.0f, +1.0f), // 22
vec3(+1.0f, +0.0f, +0.0f), // Tangent
vec3(+0.0f, -1.0f, +0.0f), // Normal
vec2(+0.0f, +0.0f), // UV
vec3(+1.0f, -1.0f, +1.0f), // 23
vec3(+1.0f, +0.0f, +0.0f), // Tangent
vec3(+0.0f, -1.0f, +0.0f), // Normal
vec2(+1.0f, +0.0f), // UV
};
unsigned short stackIndices[] = {
0, 1, 2, 0, 2, 3, // Top
4, 5, 6, 4, 6, 7, // Front
8, 9, 10, 8, 10, 11, // Right 
12, 13, 14, 12, 14, 15, // Left
16, 17, 18, 16, 18, 19, // Back
20, 22, 21, 20, 23, 22, // Bottom
};
GLuint vertBufferSize=24*sizeof(Vertex);
GLuint indexBufferSize=36*sizeof(uint);
theShadeInfo[4]=genWindow->createShaderInfo("TangentSpaceVShader.glsl","TangentSpaceFShader.glsl");
thegeom[4]=genWindow->addGeometry(stackVerts,vertBufferSize,indexBufferSize,stackIndices,36,GL_TRIANGLES);
	genWindow->addShaderStreamedParameter(thegeom[4],0,GeneralWindows::ParameterType::PT_VEC3,11*sizeof(float),0);
	genWindow->addShaderStreamedParameter(thegeom[4],1,GeneralWindows::ParameterType::PT_VEC3,11*sizeof(float),3*sizeof(float));
	genWindow->addShaderStreamedParameter(thegeom[4],2,GeneralWindows::ParameterType::PT_VEC3,11*sizeof(float),6*sizeof(float));
	genWindow->addShaderStreamedParameter(thegeom[4],3,GeneralWindows::ParameterType::PT_VEC2,11*sizeof(float),9*sizeof(float));
	theRend[5]=genWindow->addRenderable(thegeom[4],customCubeTransform,theShadeInfo[4],thetexture[2]);

}
void BinaryAlphaScene::makeNoiseTex()
{
			int width = 128;
			int height = 128;
			noise::module::Perlin perlinNoise;
			// Base frequency for octave 1.
			perlinNoise.SetFrequency(3.0);
			GLubyte *data = new GLubyte[ width * height * 4 ];
			double xRange = 1.0;
			double yRange = 1.0;
			double xFactor = xRange / width;
			double yFactor = yRange / height;
			for( int oct = 0; oct < 4; oct++ ) {
			 perlinNoise.SetOctaveCount(oct+1);
			 for( int i = 0; i < width; i++ ) {
				 for( int j = 0 ; j < height; j++ ) {
				 double x = xFactor * i;
				 double y = yFactor * j;
				 double z = 0.0;
					float val = 0.0f;
					 double a, b, c, d;
					 a = perlinNoise.GetValue(x ,y ,z);
					 b = perlinNoise.GetValue(x+xRange,y ,z);
					 c = perlinNoise.GetValue(x ,y+yRange,z);
					 d = perlinNoise.GetValue(x+xRange,y+yRange,z);
					 double xmix = 1.0 - x / xRange;
					 double ymix = 1.0 - y / yRange;
					 double x1 = glm::mix( a, b, xmix );
					 double x2 = glm::mix( c, d, xmix );
					val = glm::mix(x1, x2, ymix );
					 // Scale to roughly between 0 and 1
					 val = (val + 1.0f) * 0.5f;
					 // Clamp strictly between 0 and 1
					 val = val> 1.0 ? 1.0 :val;
					 val = val< 0.0 ? 0.0 :val;
				 // Store in texture
				 data[((j * width + i) * 4) + oct] = 
				 (GLubyte) ( val * 255.0f );
				 }
			 }
			}

			glGenTextures(1, &texID);
			glBindTexture(GL_TEXTURE_2D, texID);
			glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,width,height,0,GL_RGBA,
			 GL_UNSIGNED_BYTE,data);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, 
			 GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, 
			 GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			delete [] data;
}
void BinaryAlphaScene::setUpFrameBuffer()
{
	glGenFramebuffers(1,&myFrameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER,myFrameBuffer);
	
	glActiveTexture(GL_TEXTURE0);

	glGenTextures(1, &myColorTextureID);
	glBindTexture(GL_TEXTURE_2D,myColorTextureID);
	glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,1024,1024,0,GL_RGBA,GL_UNSIGNED_BYTE,0);
	glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_TEXTURE_2D,myColorTextureID,0);

	glGenTextures(1, &myDepthTextureID);
	glBindTexture(GL_TEXTURE_2D,myDepthTextureID);
	glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D,0,GL_DEPTH_COMPONENT32,1024,1024,0,GL_DEPTH_COMPONENT,GL_FLOAT,0);
	glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_TEXTURE_2D,myDepthTextureID,0);
	
	GLuint status= glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
	if(status!=GL_FRAMEBUFFER_COMPLETE)
		qDebug()<< "Frame buffer not complete...";
}