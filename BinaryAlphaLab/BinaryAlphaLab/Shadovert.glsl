#version 400
in layout(location=0) vec3 position;
in layout(location=1) vec4 Color;
in layout(location=2) vec3 normal;
in layout(location=3) vec2 uv;


uniform mat4 transform;
uniform mat4 shadowMatrix;
uniform mat4 ModelMatrix;

out vec4 shadowUv;
out vec3 inNormal;
out vec3 inPosition;
out vec2 inUv;
void main()
{
	vec4 p=vec4(position,1);
	vec4 worldpos=ModelMatrix*p;
	shadowUv=shadowMatrix*worldpos;
	inUv=uv;
	inNormal=normal;
	inPosition=position;
	
	vec4 newPosition=transform*p;
	gl_Position=newPosition;
	
	
	
}