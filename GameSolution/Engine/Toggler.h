#pragma once
#include "ExportHeader.h"
#include "..\..\..\zExternalDependencies\glm\glm\glm.hpp"
#include <QtGui\qlabel.h>
#include <QtGui\qcheckbox.h>
struct Toggler
{
	bool *check;
	QCheckBox * value; 
	ENGINE_SHARED void init(const char* varableName, bool &floatWatched);
	ENGINE_SHARED void update();
};