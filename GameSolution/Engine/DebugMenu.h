#pragma once
#include "ExportHeader.h"
#include <QtGui\QHboxlayout>
#include <QtGui\QVboxlayout>
#include <QtGui\qpushbutton.h>
#include <QtGui\qlabel.h>
#include <QtGui\qcheckbox.h>
#include "DEBUG_SLIDER.h" 
#include <QtGui\Qboxlayout.h>
#include <QtGui\qtabwidget.h>

#include "Watcher.h"
#include "Slider.h"
#include "Toggler.h"
#include "Pusher.h"
#include "MyTab.h"
using std::vector;

class DebugMenu 
{
/*
singleton

*/
	static DebugMenu theInstance;
	DebugMenu(){}
	DebugMenu(const DebugMenu&);
	DebugMenu operator = (const DebugMenu&);
	~DebugMenu(){}
public:
	
	ENGINE_SHARED static DebugMenu& getInstance();
	
	ENGINE_SHARED void initialize(QHBoxLayout *fullLayout);

	vector<Watcher*> watchers;
	vector<Slider*> sliders;
	vector<Toggler*> togglers;
	vector<Pusher*> pushers;
	vector<MyTab*> myTabs;

	QVBoxLayout *watcherCol;
	QVBoxLayout *sliderCol;
	QVBoxLayout *togglerCol;
	QVBoxLayout *pusherCol;
	QTabWidget * tabWidge;

ENGINE_SHARED void watchFloat(const char* varableName, float &floatWatched,const char* fileName);
ENGINE_SHARED void slideFloat(const char* varableName, float &floatAltered, float min, float max,const char* fileName);
ENGINE_SHARED void toggleBool(const char* varableName, bool	 &valueChanged,const char* fileName);
ENGINE_SHARED void addTab(const char* fileName);
ENGINE_SHARED int tabExists(const char* fileName);
ENGINE_SHARED void button(const char* varableName, fastdelegate::FastDelegate0<> func,const char* fileName);
ENGINE_SHARED void updateValues();
};
#define DebugMe DebugMenu::getInstance()