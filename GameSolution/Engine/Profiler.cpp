#include "Profiler.h"

Profiler Profiler::theInstance;
Profiler& Profiler::getInstance()
{
	return theInstance;
}

#ifdef PROFILING_ON

#include <fstream>

static std::ofstream outStream;

void Profiler::startUp(const char* fileName)
{
	this->fileName = fileName;
	frameIndex=-1;
	
	categoryIndex=0;
}

void Profiler::shutdown()
{
	writeData();
}

void Profiler::newFrame()
{
	
	if(frameIndex > 0)
	{
		//Assert(categoryIndex==numUsedCategories);
	}
	frameIndex++;
	categoryIndex=0;
	
}
void Profiler::addEntry(const char* category,float time)
{
	
	//Assert(categoryIndex < MAX_PROFILE_CATEGORIES);
	ProfileCategory& pc = categories[categoryIndex];

	if(frameIndex == 0)
	{
		pc.name=category;
		numUsedCategories++;
		
	}
	else
	{
		
		//Assert(pc.name==category && category != NULL);
		
	}
	categoryIndex++;
	pc.samples[frameIndex % MAX_FRAME_SAMPLES] = time;
}

char  Profiler::getDelimiter(unsigned int index) const
{
	 return (((index + 1) < numUsedCategories) ? ',' :'\n');
}

bool Profiler::wrapped() const
{
	return frameIndex >= MAX_FRAME_SAMPLES && frameIndex != -1;
}
void Profiler::writeData() const
{
	
	outStream.open(fileName,std::ios::trunc);
	//Write category headers
	for(unsigned int i = 0;i < numUsedCategories; i++)
	{
		outStream << categories[i].name;
		outStream << getDelimiter(i);
		
	}
	
	int endIndex;
	int startIndex;
	if (wrapped())
	{
		endIndex = frameIndex % MAX_FRAME_SAMPLES;
		startIndex = (endIndex + 1) % MAX_FRAME_SAMPLES;
		while(startIndex != endIndex)
		{
			writeFrame(startIndex);
			startIndex = (startIndex + 1) %  MAX_FRAME_SAMPLES;
		}
		if(currentFrameComplete())
		{
			writeFrame(startIndex);
		}
	}

	else
	{
		unsigned int numActualFrames = frameIndex;
		if(currentFrameComplete())
		{
			numActualFrames++;
		}

		startIndex=0;
		endIndex=numActualFrames;
		while(startIndex < endIndex)
		{
			writeFrame(startIndex++);

		}
	}
	outStream.close();
}

void Profiler::writeFrame(int frameNumber) const
{
	for( unsigned int cat = 0; cat < numUsedCategories; cat++)
	{
		outStream << categories[cat].samples[frameNumber];
		outStream << getDelimiter(cat);
	}
}

bool Profiler:: currentFrameComplete() const
{
	return categoryIndex ==numUsedCategories;
}


#endif
