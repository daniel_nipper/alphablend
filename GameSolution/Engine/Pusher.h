#pragma once
#include "ExportHeader.h"
#include <QtGui\qpushbutton.h>
#include "FastDelegate.h"

struct Pusher : public QObject
{
public:
	fastdelegate::FastDelegate0<> delegate;
	QPushButton *button;
	ENGINE_SHARED void init(const char* varableName, fastdelegate::FastDelegate0<>);
	ENGINE_SHARED void update();

};

