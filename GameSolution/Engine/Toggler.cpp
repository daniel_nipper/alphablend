#include "Toggler.h"

void Toggler::init(const char* varName, bool & theCheck)
{
	
	check = &theCheck;
	value = new QCheckBox(varName);
	value -> setChecked(*check);
}

void Toggler::update()
{
	*check =  value -> isChecked();
}