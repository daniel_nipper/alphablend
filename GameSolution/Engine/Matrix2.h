#ifndef Matrix2_H
#define Matrix2_H
#include "Vector2.h"
const struct Matrix2D
{
	Vector2D vec1;
	Vector2D vec2;
Matrix2D(Vector2D incom1,Vector2D incom2)
{
  vec1=incom1;
  vec2=incom2;
}

inline friend Vector2D operator*(const Matrix2D& mate, const Vector2D& vic);
};
inline Vector2D operator*(const Matrix2D& mate, const Vector2D& vic)
{
	return Vector2D(mate.vec1.x*vic.x+mate.vec2.x*vic.y,mate.vec1.y*vic.x+mate.vec2.y*vic.y);
	
}
#endif