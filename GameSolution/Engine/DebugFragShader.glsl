#version 400

in vec3 deColor;
out vec3 theFinalColor;

void main()
{
	theFinalColor = deColor;
};