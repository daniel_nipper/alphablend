#pragma once
#include "ExportHeader.h"
#include "..\..\..\zExternalDependencies\glm\glm\glm.hpp"
#include <QtGui\qlabel.h>
#include "DEBUG_SLIDER.h" 
struct Slider
{
	float *value;
	QLabel *name;
	DebugSlider *valueSlider;
	ENGINE_SHARED void init(const char* varableName, float &floatAltered, float min, float max);
	ENGINE_SHARED void update();
};