#pragma once
#include "Colorness.h"
#include "Particle.h"
class ParticleEffect
{
	Particle* particles;


	int numParticles;
	Randomness randy;
	Colorness myColor;
	int particalType;
	
	
public:
	
	ENGINE_SHARED ParticleEffect();
	ENGINE_SHARED ParticleEffect(Vector2D origin, Core::RGB color,int numberOFParticles,int type,float lifeTime);
	ENGINE_SHARED ParticleEffect(Vector2D origin,Vector2D multiplyer,Core::RGB color,int numberOFParticles,int type,float lifeTime);
	ENGINE_SHARED ~ParticleEffect();
	ENGINE_SHARED void Update(float dt);
	ENGINE_SHARED void Draw(Core::Graphics& graphics);
	
};

