#ifndef Vector2_H
#define Vector_H
#include <cmath>


const struct Vector2D
{

	float x;
	float y;

	Vector2D(float x=0, float y=0) : x(x),y(y)
	{
	}

operator float*()
{

	return &x;
}

inline friend Vector2D operator +(const Vector2D& left, const Vector2D& right);
inline friend Vector2D operator -(const Vector2D& left, const Vector2D& right);
inline friend Vector2D operator *(float scalar, const Vector2D& right); 
inline friend Vector2D operator *(const Vector2D& right, float scalar); 
//inline friend Vector2D operator *( const Vector2D& right,const Vector2D& left); 
////inline friend Vector2D operator /(Vector2D& left, const float length);

inline  Vector2D PerpCW()
{
  
	return Vector2D(y,-x);
}
inline Vector2D PerpCCW()
{
  
	return Vector2D(-y,x);

}
inline float Dot(Vector2D& newVector)
{
   return (x *newVector.x +y * newVector.y);
}

inline Vector2D coss(Vector2D other)
{
	return Vector2D(y-other.y,x-other.x); 
}

inline Vector2D lerp(float number, const Vector2D& newVector)
{
  return Vector2D((1-number)*x+(number*newVector.x),(1-number)*y+(number*newVector.y));
}

inline float length()
{
  return sqrt((x*x)+(y*y));
}

inline float lengthSquared()
{
 return length()*length();
}

inline Vector2D normalized()
{
 return Vector2D(x/length(),y/length());

}
inline Vector2D projectOnto(Vector2D left, Vector2D right)
{
   return Vector2D(left.Dot(right)/left.lengthSquared()*left);
}

};


inline Vector2D operator +(const Vector2D& left, const Vector2D& right)
{
	return Vector2D(left.x + right.x,left.y + right.y);
}
inline Vector2D operator -(const Vector2D& left, const Vector2D& right)
{
	return Vector2D(left.x - right.x,left.y - right.y);
}
inline Vector2D operator *(float scalar,const Vector2D& right )
{
	return Vector2D(scalar* right.x,scalar* right.y);
}
inline Vector2D operator *(const Vector2D& right,float scalar )
{
	return Vector2D(scalar* right.x,scalar* right.y);
}

inline Vector2D operator *(const Vector2D& right, const Vector2D& left)
{
	return Vector2D( right.x*left.x, right.y*left.y);
}
//inline Vector2D operator /(Vector2D& left, const float length)
//{
//	return Vector2D(left.x/length, left.y/length);
//}

#endif