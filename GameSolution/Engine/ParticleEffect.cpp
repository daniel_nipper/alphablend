#include "Vector2.h"
#include "Core.h"
using Core::RGB;
#include "MemoryDebug.h"
#include "ParticleEffect.h"
#include "Profile.h"
#include "Assert.h"

float partLifeTime;
Vector2D trail;
ParticleEffect::ParticleEffect()
{}
ParticleEffect::ParticleEffect(Vector2D origin, Core::RGB color,int numberOFParticles,int type,float lifeTime)
{

	particalType=type;
	numParticles=numberOFParticles;
	partLifeTime=lifeTime;
	particles= new Particle[numParticles];

	if(particalType==1)
	{
		for(int i=0;i<numParticles;i++)
		{
			particles[i].particalPosition=origin;
			particles[i].particalVelocity=randy.randomUnitVector()*randy.randomInRange(1,500);
			particles[i].particleColor=myColor.varyColor(color,50);
			particles[i].particalLifeTime=lifeTime+=.005f;

		}
	}
	if(particalType==2)
	{
		for(int i=0;i<numParticles;i++)
		{
			particles[i].particalPosition=origin;
			particles[i].particalVelocity=(randy.randomInRange(1,500)*randy.randomUnitVector())*2;
			particles[i].particleColor=myColor.varyColor(color,100);
			particles[i].particalLifeTime=lifeTime;

		}
	}
//	Assert(particalType == 1 || particalType == 2);
}
ParticleEffect::ParticleEffect(Vector2D origin,Vector2D multiplyer,Core::RGB color,int numberOFParticles,int type,float lifeTime)
{
	float move=0;
	particalType=type;
	numParticles=numberOFParticles;
	partLifeTime=lifeTime;
	particles= new Particle[numParticles];

	if(particalType==1)
	{
		for(int i=0;i<numParticles;i++)
		{
			particles[i].particalPosition=origin;
			particles[i].particalVelocity=randy.randomUnitVector()*randy.randomInRange(1,500);
			particles[i].particleColor=myColor.varyColor(color,50);
			particles[i].particalLifeTime=lifeTime+=.005f;

		}
	}
	if(particalType==2)
	{
		for(int i=0;i<numParticles;i++)
		{
			particles[i].particalPosition=origin;
			particles[i].particalVelocity=(multiplyer +Vector2D(move,move))*randy.randomInRange(1,5000);
			particles[i].particleColor=myColor.varyColor(color,100);
			particles[i].particalLifeTime=lifeTime+=.005f;
			move+=.0005f;
		}
	}
//	Assert(particalType == 1 || particalType == 2);
}

ParticleEffect::~ParticleEffect()
{
	delete[] particles;
}
void ParticleEffect::Update(float dt)
{
	{
		PROFILE("particle Update");

		for(int i=0;i<numParticles;i++)
		{
			if(particalType ==1)
			{
				particles[i].particalPosition = particles[i].particalPosition + particles[i].particalVelocity * dt;

				particles[i].particalLifeTime-=dt;
			}
			else
			{
				particles[i].particalPosition = dt*particles[i].particalPosition + particles[i].particalVelocity;
			}
		}
	}
}
void ParticleEffect::Draw(Core::Graphics& graphics)
{
	{
		PROFILE("particle draw");
		Vector2D pointA;
		Vector2D pointB;
		for(int i=0;i<numParticles;i++)
		{
			if(particles[i].particalLifeTime>0)
			{

				graphics.SetColor(particles[i].particleColor);
				pointA=particles[i].particalPosition;
				pointB=particles[i].particalPosition + randy.randomUnitVector();
				graphics.DrawLine(pointA.x,pointA.y,pointB.x,pointB.y);
			}
		}
	}
}
