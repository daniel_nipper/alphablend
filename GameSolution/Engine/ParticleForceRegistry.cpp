#include "ParticleForceRegistry.h"


void ParticleForceRegistry::add(Particle *particle,ParticleForceGenerator *fg)
{
	ParticleForceRegistration temp;
	temp.fg=fg;
	temp.particle=particle;
	registrations.push_back(temp);
}
void ParticleForceRegistry::remove(Particle *particle, ParticleForceGenerator *fg)
{
	ParticleForceRegistration temp;
	temp.fg=fg;
	temp.particle=particle;
	Registry::iterator i =registrations.begin();
	for (; i!= registrations.end(); i++)
	{
		if(i->fg==temp.fg &&i->particle==temp.particle)
		{
			registrations.erase(i);
		}

	}
	
}
void ParticleForceRegistry::clear()
{
	Registry::iterator i =registrations.begin();
	for (; i!= registrations.end(); i++)
	{
		
			registrations.erase(i);
		
	}
}
void ParticleForceRegistry::updateForces(float duration)
{
	Registry::iterator i =registrations.begin();
	for (; i!= registrations.end(); i++)
	{
		i->fg->updateForce(i->particle,duration);
	}
}
