#pragma once
#include "ExportHeader.h"
#include "Particle.h"
#include "ParticleForceGenerator.h"
#include <vector>
class ParticleForceRegistry
{
protected:
	struct ParticleForceRegistration
	{
		Particle *particle;
		ParticleForceGenerator *fg;
	};
	typedef std::vector<ParticleForceRegistration> Registry;
	Registry registrations;
public:
	void ENGINE_SHARED add(Particle *particle,ParticleForceGenerator *fg);
	void ENGINE_SHARED remove(Particle *particle, ParticleForceGenerator *fg);
	void ENGINE_SHARED clear();
	void ENGINE_SHARED updateForces(float duration);
	
};

