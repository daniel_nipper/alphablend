#pragma once
#include "ExportHeader.h"
#include "..\..\zExternalDependencies\glm\glm\glm.hpp"
class Camera
{
	
	const glm::vec3 UP;
	glm::vec2 oldMousePosition;
	glm::vec3 strafeDirection;
public:
	glm::vec3 viewDirection;
	glm::vec3 position;
	ENGINE_SHARED Camera();
	ENGINE_SHARED void mouseUpdate(const glm::vec2& newMousePosition);
	ENGINE_SHARED glm::mat4 getWorldToViewMatrix() const;
	ENGINE_SHARED void moveFoward();
    ENGINE_SHARED void movBackward();
	ENGINE_SHARED void strafeRight();
	ENGINE_SHARED void strafeLeft();
	ENGINE_SHARED void moveUp();
	ENGINE_SHARED void moveDown();
};

