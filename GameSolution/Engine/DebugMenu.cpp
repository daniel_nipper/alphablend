#include "DebugMenu.h"
#include <sstream>
#include <string.h>
using std::string;
DebugMenu DebugMenu::theInstance;

DebugMenu& DebugMenu::getInstance(){return theInstance;}
void DebugMenu::initialize(QHBoxLayout *fullLayout)
{	
	tabWidge=new QTabWidget();
	watcherCol = new QVBoxLayout();
	sliderCol = new QVBoxLayout();
	togglerCol = new QVBoxLayout();
	pusherCol = new QVBoxLayout();
	//tabWidge->setFixedHeight(50);
	fullLayout->addWidget(tabWidge);
	/*fullLayout ->addLayout(watcherCol);
	fullLayout ->addLayout(sliderCol);
	fullLayout ->addLayout(togglerCol);
	fullLayout ->addLayout(pusherCol);*/

}

void DebugMenu::watchFloat(const char* name, float &watched,const char* fileName)
{
	Watcher *newMember = new Watcher();
	newMember ->init(name, watched);
	watchers.push_back(newMember);

	QHBoxLayout *newRow = new QHBoxLayout();
	newRow -> addWidget(newMember->name);
	newRow -> addWidget(newMember->value);
	int existed = tabExists(fileName);
	if(existed==-1)
	{
		addTab(fileName);
		existed = tabExists(fileName);
		myTabs.at(existed)->tabWidget->addLayout(newRow);
	}
	else
	{
		myTabs.at(existed)->tabWidget->addLayout(newRow);
	}
	//watcherCol -> addLayout(newRow);
}

void DebugMenu::slideFloat(const char* name, float &watched, float min, float max,const char* fileName)
{
	Slider *newMember = new Slider();
	newMember ->init(name, watched, min, max);
	sliders.push_back(newMember);

	QHBoxLayout *newRow = new QHBoxLayout();
	newRow -> addWidget(newMember->name);
	newRow -> addWidget(newMember->valueSlider);
	int existed = tabExists(fileName);
	if(existed==-1)
	{
		addTab(fileName);
		existed = tabExists(fileName);
		myTabs.at(existed)->tabWidget->addLayout(newRow);
	}
	else
	{
		myTabs.at(existed)->tabWidget->addLayout(newRow);
	}
	//sliderCol -> addLayout(newRow);
}

void DebugMenu::toggleBool(const char* name, bool &watched,const char* fileName)
{
	Toggler *newMember = new Toggler();
	newMember ->init(name,watched);
	togglers.push_back(newMember);

	QHBoxLayout *newRow = new QHBoxLayout();
	
	newRow -> addWidget(newMember ->value);
	int existed = tabExists(fileName);
	if(existed==-1)
	{
		addTab(fileName);
		existed = tabExists(fileName);
		myTabs.at(existed)->tabWidget->addLayout(newRow);
	}
	else
	{
		myTabs.at(existed)->tabWidget->addLayout(newRow);
	}
	//togglerCol -> addLayout(newRow);

}

void DebugMenu::button(const char* name, fastdelegate::FastDelegate0<> func,const char* fileName)
{
	Pusher *newMember = new Pusher();
	newMember ->init(name, func);
	pushers.push_back(newMember);

	QHBoxLayout *newRow = new QHBoxLayout();
	
	newRow -> addWidget(newMember ->button);
	int existed = tabExists(fileName);
	if(existed==-1)
	{
		addTab(fileName);
		existed = tabExists(fileName);
		myTabs.at(existed)->tabWidget->addLayout(newRow);
	}
	else
	{
		myTabs.at(existed)->tabWidget->addLayout(newRow);
	}
	//pusherCol -> addLayout(newRow);
}
void DebugMenu::addTab(const char* fileName)
{
	MyTab * newTab=new MyTab();
	newTab->tabName=fileName;
	newTab->tabWidget=new myQWidget();
	myTabs.push_back(newTab);
	tabWidge->addTab(newTab->tabWidget,newTab->tabName);
}
int DebugMenu::tabExists(const char* fileName)
{
	int existance=-1;
	string filn=fileName;
	for (int i = 0; i < myTabs.size(); i++)
	{
		string chosen=myTabs.at(i)->tabName;
		if(filn==chosen)
		{
			existance=i;
		}
	}
	return existance;
}
 void DebugMenu::updateValues()
 {
	 for(int i =0; i < watchers.size();i++)
	 {
		 watchers.at(i)->update();
	 }

	 for(int i =0; i < sliders.size();i++)
	 {
		 sliders.at(i)->update();
	 }

	 for(int i =0; i < togglers.size();i++)
	 {
		 togglers.at(i)->update();
	 }

	  for(int i =0; i < pushers.size();i++)
	 {
		 pushers.at(i)->update();
	 }
 }