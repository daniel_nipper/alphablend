#include "Watcher.h"
#include <Qt\qstring.h>
#include <sstream>
void Watcher::init(const char* varableName, float &floatWatched)
{
	name = new QLabel();
	value = new QLabel();
	num  = &floatWatched;
	std::stringstream ss;
	ss << *num;
	name -> setText(varableName);
	value -> setText(ss.str().c_str());
}

void Watcher::update()
{
	std::stringstream ss;
	ss << *num;
	value ->setText(ss.str().c_str());

}