#pragma once
#include "ExportHeader.h"
#include <Qt\qwidget.h>
#include <QtGui\QHboxLayout>
#include <QtGui\QVboxLayout>
struct myQWidget: public QWidget
{
	QVBoxLayout* mainLayout;
	ENGINE_SHARED myQWidget();
	ENGINE_SHARED void addLayout(QHBoxLayout* inLayout);
};
struct MyTab
{
	const char* tabName;
	myQWidget* tabWidget;
};
