#pragma once 
#include "ExportHeader.h"
#include "..\..\..\zExternalDependencies\glm\glm\glm.hpp"
#include <QtGui\qlabel.h>
struct Watcher
{
	float *num;
	QLabel *name;
	QLabel *value;
	ENGINE_SHARED void init(const char* varableName, float &floatWatched);
	ENGINE_SHARED void update();

};