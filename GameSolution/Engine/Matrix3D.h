#ifndef Matrix3D_H
#define Matrix3D_H
#include "Vector3D.h"
//#include "Vector2.h"
#include "Matrix2.h"
#include <cmath>
struct Matrix3D
{
	
	float matrix[3][3];
	Matrix3D(Vector3D inOne, Vector3D inTwo,Vector3D  inThree)
	{
		matrix[0][0]=inOne.x;
		matrix[1][0]=inOne.y;
		matrix[2][0]=inOne.z;

		matrix[0][1]=inTwo.x;
		matrix[1][1]=inTwo.y;
		matrix[2][1]=inTwo.z;

		matrix[0][2]= inThree.x;
		matrix[1][2]= inThree.y;
		matrix[2][2]= inThree.z;
	}
	Matrix3D(Vector2D inOne, Vector2D inTwo)
	{
		matrix[0][0]=inOne.x;
		matrix[1][0]=inOne.y;
		matrix[2][0]=0;

		matrix[0][1]=inTwo.x;
		matrix[1][1]=inTwo.y;
		matrix[2][1]=0;

		matrix[0][2]= 0;
		matrix[1][2]= 0;
		matrix[2][2]= 1;
	}

	Matrix3D()
	{
		matrix[0][0]=1;
		matrix[1][0]=0;
		matrix[2][0]=0;
		matrix[0][1]=0;
		matrix[1][1]=1;
		matrix[2][1]=0;
		matrix[0][2]=0;
		matrix[1][2]=0;
		matrix[2][2]=1;
	}
	
	operator float*()
{

	return &matrix[0][0];
}
Matrix3D scalor(const float& sx, const float& sy)
	{
		Matrix3D scale;
		
		scale.matrix[0][0]=sx;
		scale.matrix[1][1]=sy;
		return scale;
	}
Matrix3D rotate(const float& angle)
	{
		Matrix3D rotater;
		float cAngle=cos(angle);
		float sAngle=sin(angle);
		rotater.matrix[0][0]=cAngle;
		rotater.matrix[1][0]=sAngle;
		rotater.matrix[0][1]=-sAngle;
		rotater.matrix[1][1]=cAngle;
		return rotater;
		
	}
	Matrix3D translate(const float& x, const float& y)
	{
		Matrix3D translater;
		translater.matrix[0][2]=x;
		translater.matrix[1][2]=y;
		return translater;

	}
		Matrix3D translateX(const float& x)
	{
		Matrix3D translaterX;
		translaterX.matrix[0][2]=x;
	
		return translaterX;

	}
};

inline Vector3D operator*(Matrix3D& mate,  Vector3D vic)
{
	Vector3D result;
	result.x=mate.matrix[0][0]*vic.x+mate.matrix[0][1]*vic.y+mate.matrix[0][2]*vic.z;
	result.y=mate.matrix[1][0]*vic.x+mate.matrix[1][1]*vic.y+mate.matrix[1][2]*vic.z;
	result.z=mate.matrix[2][0]*vic.x+mate.matrix[2][1]*vic.y+mate.matrix[2][2]*vic.z;
	return result;
	
}

inline Vector2D operator*(Matrix3D& mate,  Vector2D& vic)
{
	Vector2D result;
	result.x=mate.matrix[0][0]*vic.x+mate.matrix[0][1]*vic.y+mate.matrix[0][2];
	result.y=mate.matrix[1][0]*vic.x+mate.matrix[1][1]*vic.y+mate.matrix[1][2];
	return result;
	
}

inline Matrix3D operator*(const Matrix3D& left, const Matrix3D& right)
{

	return Matrix3D(
		Vector3D(
			left.matrix[0][0]*right.matrix[0][0]+left.matrix[0][1]*right.matrix[1][0]+left.matrix[0][2]*right.matrix[2][0],
			left.matrix[1][0]*right.matrix[0][0]+left.matrix[1][1]*right.matrix[1][0]+left.matrix[1][2]*right.matrix[2][0],
			left.matrix[2][0]*right.matrix[0][0]+left.matrix[2][1]*right.matrix[1][0]+left.matrix[2][2]*right.matrix[2][0]),

		Vector3D(
			left.matrix[0][0]*right.matrix[0][1]+left.matrix[0][1]*right.matrix[1][1]+left.matrix[0][2]*right.matrix[2][1],
			left.matrix[1][0]*right.matrix[0][1]+left.matrix[1][1]*right.matrix[1][1]+left.matrix[1][2]*right.matrix[2][1],
			left.matrix[2][0]*right.matrix[0][1]+left.matrix[2][1]*right.matrix[1][1]+left.matrix[2][2]*right.matrix[2][1]),
		
		Vector3D(
			left.matrix[0][0]*right.matrix[0][2]+left.matrix[0][1]*right.matrix[1][2]+left.matrix[0][2]*right.matrix[2][2],
			left.matrix[1][0]*right.matrix[0][2]+left.matrix[1][1]*right.matrix[1][2]+left.matrix[1][2]*right.matrix[2][2],
			left.matrix[2][0]*right.matrix[0][2]+left.matrix[2][1]*right.matrix[1][2]+left.matrix[2][2]*right.matrix[2][2]));
		
}

#endif
