#pragma once
#include "../../zExternalDependencies/glm/glm/glm.hpp"
#include "ExportHeader.h"
//#include "Core.h"

class Particle
{
public:
	glm::vec3 particalPosition;
	glm::vec3 particalVelocity;
	glm::vec3 forceAccum;
	glm::vec3 penetrationChange;
	float acceleration;
	float damping;
	float mass;
	float percent;
	float  particalLifeTime;
	void ENGINE_SHARED clearAccumulator();
	void ENGINE_SHARED addForce(const glm::vec3 &force);
};

