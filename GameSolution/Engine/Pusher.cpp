#include "Pusher.h"

void Pusher::init(const char* varableName, fastdelegate::FastDelegate0<> d)
{
	button = new QPushButton(varableName);
	delegate = d;
}


void Pusher::update()
{
	if(button-> isDown())
	{
		delegate();
	}
}
