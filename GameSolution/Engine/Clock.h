#pragma once
#include "ExportHeader.h"
#include <Windows.h>
class Clock
{
	LARGE_INTEGER timeFrequency;
	LARGE_INTEGER timeLastFrame;
	LARGE_INTEGER deltaLastFrame;
	float deltaTimeRecip;
	float deltaTime;
public:


	ENGINE_SHARED void start();
	ENGINE_SHARED bool initialize();
	ENGINE_SHARED bool shutDown();
	ENGINE_SHARED float lap();
	ENGINE_SHARED float stop();
	ENGINE_SHARED float   lastLapTime() const;
	ENGINE_SHARED float miliSeconds();
};

