#pragma once
#include "ExportHeader.h"
#include "Assert.h"
class Profiler
{
public:
	static const unsigned int MAX_FRAME_SAMPLES = 500;

	ENGINE_SHARED static Profiler& getInstance();
private:
	ENGINE_SHARED Profiler(){}
	ENGINE_SHARED Profiler(const Profiler&);
	ENGINE_SHARED Profiler& operator =(const Profiler&);
	static Profiler theInstance;
	
#if PROFILING_ON
	const char* fileName;
	static const unsigned int MAX_PROFILE_CATEGORIES=20;
	int frameIndex;
	unsigned int categoryIndex;
	unsigned int numUsedCategories;
	struct ProfileCategory
	{
		const char* name;
		float samples[MAX_FRAME_SAMPLES];
	}categories[MAX_PROFILE_CATEGORIES];
	ENGINE_SHARED void writeData() const;
	ENGINE_SHARED void writeFrame(int frameNumber) const;
	ENGINE_SHARED char getDelimiter(unsigned int index)const;
	ENGINE_SHARED bool currentFrameComplete() const;
	ENGINE_SHARED bool wrapped() const;
	
#endif
public:
#if PROFILING_ON
	ENGINE_SHARED void startUp(const char * fileName);
	ENGINE_SHARED void shutdown();
	ENGINE_SHARED void newFrame();
	ENGINE_SHARED void addEntry(const char* category,float time);
#else
	ENGINE_SHARED void startUp(const char * fileName){}
	ENGINE_SHARED void shutdown(){}
	ENGINE_SHARED void newFrame(){}
	ENGINE_SHARED void addEntry(const char* category, float time){}
#endif
};

#define profiler Profiler::getInstance()

