#if PROFILING_ON
#include "Profile.h"
#include "Profiler.h"
	Profile::Profile(const char* category):category(category)
	{
		clockOne.initialize();
		clockOne.start();
		
	}
	Profile::~Profile()
	{
		clockOne.stop();
		profiler.addEntry(category,clockOne.miliSeconds());
		
	}
#endif