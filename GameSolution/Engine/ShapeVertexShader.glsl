#version 400
in layout(location=0) vec3 position;

uniform mat4 transform;
uniform vec3 Color;

out vec3 deColor;

void main()
{
vec4 p= vec4(position,1);
vec4 newPosition= transform*p;
gl_Position=newPosition;
deColor=Color;
}