//#pragma warning ( disable : 4201)
//#pragma warning ( disable : 4127)

#include "GeneralWindows.h"
#include <Qt\qdebug.h>
#include <iostream>
#include <fstream>
using std::string;
using std::cout;
using std::ifstream;
using std::istreambuf_iterator;
int geometryNum=0;	
 int currentArrayNum=-1;
 int currentShaderNum=0;
 int currentTextNum=0;
 int currentRender=0;
 GLuint megaByte=1048576;
 GLuint offset=0;
 GLuint offset2=0;

GeneralWindows::GeneralWindows()
{
}


GeneralWindows::~GeneralWindows()
{
}
 GeneralWindows::GeometryInfo* GeneralWindows::addGeometry(
		const void* verts, 
		GLuint vertexDataSize,
		GLuint indexDataSize,
		ushort* indices, uint numIndices,
		GLuint indexingMode)
{
	
	if(currentArrayNum==-1)
	{
		
		currentArrayNum=0;
		meBufferInfo[currentArrayNum].remainingSize=megaByte;
		
	}
	else if(meBufferInfo[currentArrayNum].remainingSize < (vertexDataSize + indexDataSize))
	{
		currentArrayNum++;
		meBufferInfo[currentArrayNum].remainingSize=megaByte;
		offset=0;
	}

	meBufferInfo[currentArrayNum].remainingSize= meBufferInfo[currentArrayNum].remainingSize-(vertexDataSize+indexDataSize);
	
	if(offset==0)
	{
		glGenBuffers(1,&meBufferInfo[currentArrayNum].glBufferID);
		glBindBuffer(GL_ARRAY_BUFFER, meBufferInfo[currentArrayNum].glBufferID);
	
		glBufferData(GL_ARRAY_BUFFER,megaByte,0, GL_STATIC_DRAW);
	}

	geometryInfos[geometryNum].bufferOffset=offset;
	glBufferSubData(GL_ARRAY_BUFFER,offset, vertexDataSize,verts);
	offset+=vertexDataSize;
	glBufferSubData(GL_ARRAY_BUFFER,offset, indexDataSize,indices);
	offset+=indexDataSize;

	glGenVertexArrays(1,&geometryInfos[geometryNum].geomitryID);
	geometryInfos[geometryNum].geomIndicies=numIndices;
	geometryInfos[geometryNum].bufferInfoID= meBufferInfo[currentArrayNum].glBufferID;
	geometryInfos[geometryNum].indexingMode=indexingMode;
	geometryInfos[geometryNum].geometryOffset=offset - indexDataSize;
	GeometryInfo& ret=geometryInfos[geometryNum];

 
	
	geometryNum++;
	return &ret;
}

  GeneralWindows::GeometryInfo* GeneralWindows::addGeometryWithBuffer(
		const void* verts, 
		GLuint vertexDataSize,
		GLuint indexDataSize,
		ushort* indices, uint numIndices,
		GLuint indexingMode,
		GLuint inBuffer)
{
	
	/*if(currentArrayNum==-1)
	{
		
		currentArrayNum=0;
		meBufferInfo[currentArrayNum].remainingSize=megaByte;
		
	}
	else if(meBufferInfo[currentArrayNum].remainingSize < (vertexDataSize + indexDataSize))
	{
		currentArrayNum++;
		meBufferInfo[currentArrayNum].remainingSize=megaByte;
		offset=0;
	}

	meBufferInfo[currentArrayNum].remainingSize= meBufferInfo[currentArrayNum].remainingSize-(vertexDataSize+indexDataSize);
	
	if(offset==0)
	{
		glGenBuffers(1,&meBufferInfo[currentArrayNum].glBufferID);
		glBindBuffer(GL_ARRAY_BUFFER, meBufferInfo[currentArrayNum].glBufferID);
	
		glBufferData(GL_ARRAY_BUFFER,megaByte,0, GL_STATIC_DRAW);
	}
*/
	
	geometryInfos[geometryNum].bufferOffset=offset2;
	glBufferSubData(GL_FRAMEBUFFER,offset2, vertexDataSize,verts);
	offset2+=vertexDataSize;
	glBufferSubData(GL_FRAMEBUFFER,offset2, indexDataSize,indices);
	offset2+=indexDataSize;

	glGenVertexArrays(1,&geometryInfos[geometryNum].geomitryID);
	geometryInfos[geometryNum].geomIndicies=numIndices;
	geometryInfos[geometryNum].bufferInfoID=inBuffer;
	geometryInfos[geometryNum].indexingMode=indexingMode;
	geometryInfos[geometryNum].geometryOffset=offset2 - indexDataSize;
	GeometryInfo& ret=geometryInfos[geometryNum];

 
	
	geometryNum++;
	return &ret;
}

 string readShaderCode(const char* fileName)
{
	
	ifstream meInput(fileName);
	if(!meInput.good())
	{
		cout<<"File failed to load..."<<fileName;
		exit(1);
	}
	return string(istreambuf_iterator<char>(meInput),
		istreambuf_iterator<char>());
}
 GeneralWindows::ShaderInfo* GeneralWindows::createShaderInfo(const char* vertexShaderFileName,
		const char* fragmentShaderFileName)
 {

	 GLuint vertexShaderID=glCreateShader(GL_VERTEX_SHADER);
	
	GLuint fragshaderVertex=glCreateShader(GL_FRAGMENT_SHADER);
	const char* adapter[1];
	string temp=""; 
	temp= readShaderCode(vertexShaderFileName);
	adapter[0]=temp.c_str();
	glShaderSource(vertexShaderID,1,adapter,0);
	temp= readShaderCode(fragmentShaderFileName);
	adapter[0]=temp.c_str();
	glShaderSource(fragshaderVertex,1,adapter,0);
	glCompileShader(vertexShaderID);
	glCompileShader(fragshaderVertex);
	

	GLint compileStatus;
	glGetShaderiv(vertexShaderID, GL_COMPILE_STATUS,&compileStatus);
	if(compileStatus != GL_TRUE)
	{
		GLint logLength;
		glGetShaderiv(vertexShaderID,GL_INFO_LOG_LENGTH,&logLength);
		char* buffer = new char[logLength];
		GLsizei bitBucket;
		glGetShaderInfoLog(vertexShaderID,logLength,&bitBucket,buffer);
		qDebug()<<buffer;
		delete[] buffer;
	}
	glGetShaderiv(fragshaderVertex, GL_COMPILE_STATUS,&compileStatus);
	if(compileStatus != GL_TRUE)
	{
		GLint logLength;
		glGetShaderiv(fragshaderVertex,GL_INFO_LOG_LENGTH,&logLength);
		char* buffer = new char[logLength];
		GLsizei biteBucket;
		glGetShaderInfoLog(fragshaderVertex,logLength,&biteBucket,buffer);
		qDebug()<<buffer;
		delete[] buffer;
	}

	shaderInfos[currentShaderNum].programID=glCreateProgram();
	glAttachShader(shaderInfos[currentShaderNum].programID,vertexShaderID);
	glAttachShader(shaderInfos[currentShaderNum].programID,fragshaderVertex);
	glLinkProgram(shaderInfos[currentShaderNum].programID);
	
	ShaderInfo& shadeRet=shaderInfos[currentShaderNum];
	
	currentShaderNum++;
	return &shadeRet;
 }
 GeneralWindows::RenderableInfo* GeneralWindows::addRenderable(GeometryInfo* whatGeometry,
		const glm::mat4& whereMatrix,
		ShaderInfo* howShaders,
		TextureInfo* texture)
 {
	 renderInfos[currentRender].whatGeometryIndex=whatGeometry;
	 renderInfos[currentRender].where=whereMatrix;
	 renderInfos[currentRender].howShaderIndex=howShaders;
	 renderInfos[currentRender].textureID=texture;
	 RenderableInfo& retRendable=renderInfos[currentRender];
	 currentRender++;
	 return &retRendable;

 }

 void GeneralWindows::addShaderStreamedParameter(GeometryInfo* thgeom,
		uint layoutLocation, 
		GeneralWindows::ParameterType parameterType,
		uint bufferStride,
		uint bufferOffset)
 {
	 
	 glBindVertexArray(thgeom->geomitryID);
	 glBindBuffer(GL_ARRAY_BUFFER,thgeom->bufferInfoID);
	
	 glEnableVertexAttribArray(layoutLocation);
	 int temp=parameterType/sizeof(float);
	 glVertexAttribPointer(layoutLocation,temp,GL_FLOAT,GL_FALSE,bufferStride,(void*)(bufferOffset + thgeom->bufferOffset));
	 glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,thgeom->bufferInfoID);
 }
 uint GeneralWindows::addRenderableUniformParameter( RenderableInfo* renderable,
		const char* name,
		GeneralWindows::ParameterType parameterType, 
		const float* value)
 {
	 glUseProgram(renderable->howShaderIndex->programID);
	 GLuint getuniformLocation=glGetUniformLocation(renderable->howShaderIndex->programID,name);

	 if(parameterType==PT_FLOAT)
	 {
		 glUniform1fv(getuniformLocation,1,value);
	 }
	 else if(parameterType==PT_VEC2)
	 {
		 glUniform2fv(getuniformLocation,1,value);
	 }
	  else if(parameterType==PT_VEC3)
	 {
		 glUniform3fv(getuniformLocation,1,value);
	 }
	  else if(parameterType==PT_VEC4 )
	 {
		 glUniform4fv(getuniformLocation,1,value);
	 }
	    else if(parameterType==PT_MAT3)
	 {
		 glUniformMatrix3fv(getuniformLocation,1,GL_FALSE,value);
	 }
	    else if(parameterType==PT_MAT4)
	 {
		 glUniformMatrix4fv(getuniformLocation,1,GL_FALSE,value);
	 }
		else if(parameterType==PT_BOOL)
	 {
		 glUniform1i(getuniformLocation,1);
 	 }
		
	
	 
	 return getuniformLocation;
 }
 uint GeneralWindows::addRenderableUniformBoolParameter( RenderableInfo* renderable,
		const char* name,
		GeneralWindows::ParameterType parameterType, 
		const bool* value)
 {
	 glUseProgram(renderable->howShaderIndex->programID);
	 GLuint getuniformLocation=glGetUniformLocation(renderable->howShaderIndex->programID,name);

	
		if(parameterType==PT_BOOL)
	 {
		 if(value)
		 {
			 glUniform1i(getuniformLocation,1);
		 }
		 else
		 {
			 glUniform1i(getuniformLocation,0);
		 }
 	 }
		
	
	 
	 return getuniformLocation;
 }
 GeneralWindows::TextureInfo* GeneralWindows::addTexture(const char* fileName)
 {
	
		 
	 QImage theImage;
	 if(!theImage.load(fileName))
	{
	cout<<"You broke it";
	}
	else
	{
		QImage madItImage = QGLWidget::convertToGLFormat(theImage);
		GLuint m_theImage;
		madItImage=madItImage.mirrored(false,true);
		glGenTextures(1,&m_theImage);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, m_theImage);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, madItImage.width(), madItImage.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, madItImage.bits());
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	
	glGenerateMipmap(GL_TEXTURE_2D);
	 
	textureInfos[currentTextNum].textureID=m_theImage;

	}
	 TextureInfo& retTexture= textureInfos[currentTextNum];
	 
	 currentTextNum++;
	 return &retTexture;
 }
 GeneralWindows::TextureInfo*  GeneralWindows::addCubeMap(const char* fileName[])
 {

			 glActiveTexture(GL_TEXTURE0);
		GLuint texID;
		glGenTextures(1, &texID);
		glBindTexture(GL_TEXTURE_CUBE_MAP, texID);
		const char * suffixes[] = { "posx", "negx", "posy",
		"negy", "posz", "negz" };
		GLuint targets[] = {
		GL_TEXTURE_CUBE_MAP_POSITIVE_X,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
		};
		for( int i = 0; i < 6; i++ ) {
			string texName = string(fileName[i]) +"_" + suffixes[i] + ".png";
			 QImage theImage;
	 if(!theImage.load(fileName[i]))
	{
	cout<<"You broke it";
	}
	else
	{
			QImage img = QGLWidget::convertToGLFormat(theImage);
			img=img.mirrored(false,true);
			
			glTexImage2D(targets[i], 0, GL_RGBA,img.width(), img.height(),0, GL_RGBA, GL_UNSIGNED_BYTE, img.bits());
			
	 }	
			

		}
		// Typical cube map settings
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER,
		GL_LINEAR);
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER,
		GL_LINEAR);
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S,
		GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T,
		GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R,
		GL_CLAMP_TO_EDGE);
		// Set the CubeMapTex uniform to texture unit 0
		textureInfos[currentTextNum].textureID=texID;

		TextureInfo& retTexture= textureInfos[currentTextNum];
	 
	 currentTextNum++;
	 return &retTexture;
		

 }
 void GeneralWindows::remake()
 {
	 currentArrayNum=-1;
	 currentShaderNum=0;
	 currentTextNum=0;
	 currentRender=0;
	 geometryNum=0;
	offset=0;
 }