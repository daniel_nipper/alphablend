#ifndef Bullet_H
#define Bullet_H 

class Bullet
	{
	public:
		Vector2D bulletPosition;
		Vector2D bulletVelocity;
		Matrix3D bulletAngle;
		Bullet();
		Bullet (Vector2D& position, Matrix3D& rotator,Vector2D& normalizedTarget );
		void Draw(Core::Graphics& graphics);
		void Update();
		Vector2D getBulletPosition();
	};
#endif