
#include "Turret.h"
#include "Profile.h"
#include "ParticleEffect.h"
#include <sstream>
#include "MemoryDebug.h"
const int ammo =100;
Vector2D turretArray[]={Vector2D(0,-10),Vector2D(-5,-5),Vector2D(5,-5),Vector2D(0,-10)};
Vector2D playerShip[]={Vector2D(-20,-20),Vector2D(-20,20),Vector2D(20,20),Vector2D(20,-20),Vector2D(0,-40),Vector2D(-20,-20)};

Bullet bulletArray[ammo];
SpaceShip  ship(5,playerShip);
Randomness rander;
int timeBeforeEnemySpawns=100;
int nextEnemy = 0;
const int numOfEnemies = 20;
int enemynumber = 0;
int scorePower=500;



Enemy* enemyArmy1=new Enemy[numOfEnemies];
Enemy* enemyArmy2=new Enemy[numOfEnemies];

ParticleEffect* explosions[1]={new ParticleEffect(Vector2D(-800,-800),RGB(0,0,255),300,1,2)};
ParticleEffect* explosions2[1]={new ParticleEffect(Vector2D(-800,-800),RGB(0,0,255),300,1,2)};


Turret::Turret(int numlines)
{
	turretlength=numlines;

	Score=0;
	NumOfLives=5;
}
Turret::~Turret()
{
	
		delete enemyArmy1;
		delete enemyArmy2;
		delete explosions[0];
		delete explosions2[0];
		
	
	
}
std::string num2str(int num) {
	std::stringstream ss;
	ss << num;
	return ss.str();
};
void Turret::draw(Core::Graphics& graphics)
{
	
	{
		PROFILE("ship draw");
		ship.Draw(graphics);
	}
	Vector2D startLine;
	Vector2D endLine;
	for(int i = 0; i<numOfEnemies;i++)
	{
		if(enemyArmy1[i].undefined != NULL)
		{
			enemyArmy1[i].Draw(graphics);
		}
		if(enemyArmy2[i].undefined != NULL)
		{
			enemyArmy2[i].Draw(graphics);
		}
	}
	for(int i=0;i<turretlength;i++)
	{
		Vector2D temp1=turretArray[i].PerpCCW();
		Vector2D temp2=turretArray[i+1].PerpCCW();
		startLine=turretTransformations*temp1;
		endLine=turretTransformations*temp2;
		graphics.DrawLine(startLine.x,startLine.y,endLine.x,endLine.y);



		for(int i=0; i<ammo;i++)
		{

			bulletArray[i].Draw(graphics);

		}
	}


	explosions[0]->Draw(graphics);
	explosions2[0]->Draw(graphics);
	graphics.DrawString(10,140, "Player Score");
	graphics.DrawString(140,140, num2str(Score).c_str());
	graphics.DrawString(10,160, "Number of Lives");
	
	
	
}

void Turret::Update(float dt)
{

	if(Core::Input::IsPressed(Core::Input::BUTTON_LEFT))
	{
		bulletArray[bulletNumber]=Bullet(shipPosition,turrentMatrix,normalizeShipToMouse);
		bulletNumber++;
		if(bulletNumber+1>=ammo)
		{
			bulletNumber=0;
		}
	}

	if(nextEnemy >=timeBeforeEnemySpawns )
	{
		if(enemyArmy1[enemynumber].undefined == NULL)
		{
			Enemy numberOne = Enemy(Vector2D(rander.randomInRange(0,900),700));
			enemyArmy1[enemynumber]= numberOne;
			
		}
		if(enemyArmy2[enemynumber].undefined == NULL)
		{
			Enemy numberTwo = Enemy(Vector2D(900,rander.randomInRange(0,700)));
			enemyArmy2[enemynumber] = numberTwo;
				
		}
		nextEnemy=0;
		enemynumber++;
		if(enemynumber +1>=numOfEnemies)
		{
			enemynumber=0;
		}
	}
	for(int i = 0; i<numOfEnemies;i++)
	{
		if(enemyArmy1[i].undefined != NULL)
		{
			enemyArmy1[i].Update(ship.getShipPosition());
		}
		if(enemyArmy2[i].undefined != NULL)
		{
			enemyArmy2[i].Update(ship.getShipPosition());
		}
	}
	if(Score==scorePower && Score>0)
	{
		scorePower+=500;
		
		timeBeforeEnemySpawns=timeBeforeEnemySpawns-20;
		
		
	}
	{
		PROFILE("ship update");

		ship.Update(dt);

	}
	for(int i=0; i<ammo;i++)
	{
		bulletArray[i].Update();
	}
	shipPosition=ship.getShipPosition();
	Vector2D mouse = Vector2D(float(Core::Input::GetMouseX()), float(Core::Input::GetMouseY()));
	Vector2D shipToMouse=mouse-shipPosition;
	normalizeShipToMouse=shipToMouse.normalized();     
	turrentMatrix=Matrix3D(normalizeShipToMouse,normalizeShipToMouse.PerpCW());
	shipTranslate= shipTranslate.translate(shipPosition.x,shipPosition.y);
	turretTransformations=shipTranslate*turrentMatrix;


	nextEnemy++;
	bulletsHittingEnemy();
	enemiesHitPlayer();
	explosions[0]->Update(dt);
	explosions2[0]->Update(dt);
_CrtMemCheckpoint(&test1);
}


void Turret::bulletsHittingEnemy()
{
	for(int i=0; i<ammo;i++)
	{

		for(int j=0;j<numOfEnemies;j++)
		{
			if(enemyArmy1[j].undefined != NULL)
			{
				if(colided(enemyArmy1[j].startPosition,bulletArray[i].getBulletPosition(),5))
				{
					delete explosions[0];
					ParticleEffect* boom= new ParticleEffect(enemyArmy1[j].startPosition,RGB(0,0,255),300,1,2);

					explosions[0]=boom;

					bulletArray[i]= Bullet();
					enemyArmy1[j]= Enemy();
					Score+=10;
				}

			}
			if(enemyArmy2[j].undefined != NULL)
			{
				if(colided(enemyArmy2[j].startPosition,bulletArray[i].getBulletPosition(),5))
				{
					delete explosions2[0];
					ParticleEffect* boom1= new ParticleEffect(enemyArmy2[j].startPosition,RGB(0,0,255),300,1,2);

					explosions2[0]=boom1;

					bulletArray[i]= Bullet();
					enemyArmy2[j]= Enemy();
					Score+=10;
				}
			}
		}
	}
}

void Turret::enemiesHitPlayer()
{
	for(int j=0;j<numOfEnemies;j++)
	{
		if(enemyArmy1[j].undefined != NULL)
		{
			if(colided(enemyArmy1[j].startPosition,ship.getShipPosition(),25))
			{
				for(int b=0;b<numOfEnemies;b++)
				{
					
						enemyArmy2[b].undefined=0;
						enemyArmy1[b].undefined=0;
					
				}
				NumOfLives--;
				
			}

			if(colided(enemyArmy2[j].startPosition,ship.getShipPosition(),25))
			{
				for(int b=0;b<numOfEnemies;b++)
				{
					
						enemyArmy1[b].undefined=0;
						enemyArmy2[b].undefined=0;
					
				}
				NumOfLives--;

			}
		}
	}

}
bool Turret::colided(Vector2D first, Vector2D second, int comparer)
{
	bool one=false;
	if (first.x-second.x>=-comparer && first.x-second.x<=comparer
		&& first.y- second.y>=-comparer && first.y- second.y<=comparer)
	{
		one=true;
	}
	return one;
}