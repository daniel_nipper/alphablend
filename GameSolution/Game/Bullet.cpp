#include "Matrix3D.h"
#include "Core.h"
#include "Bullet.h"

	Vector2D bullet[]={Vector2D(-2,-3),Vector2D(-2,3),Vector2D(2,3),Vector2D(2,-3),Vector2D(-2,-3)};
	Bullet::Bullet()
		{bulletPosition=Vector2D(-100,-100);}
	Bullet::Bullet (Vector2D& position, Matrix3D& rotator,Vector2D& normalizedTarget )
		{
			bulletPosition=position;
			bulletVelocity=normalizedTarget;
			bulletAngle=rotator;
		}
		void Bullet::Draw(Core::Graphics& graphics)
		{
			
				int NumberLines=4;
				Matrix3D bulletTranslate=bulletTranslate.translate(bulletPosition.x,bulletPosition.y);
				Matrix3D bulletTransformations=bulletTranslate*bulletAngle;
				for(int i=0;i<NumberLines;i++)
				{
					Vector2D temp3=bullet[i].PerpCCW();
					Vector2D temp4=bullet[i+1].PerpCCW();
					Vector2D bulletStart=bulletTransformations*temp3;
					Vector2D bulletEnd=bulletTransformations*temp4;
					graphics.DrawLine(bulletStart.x,bulletStart.y,bulletEnd.x,bulletEnd.y);
				}
				graphics.SetColor(RGB(10,125,200));
		}
		void Bullet::Update()
		{
			
				bulletPosition=bulletPosition+bulletVelocity;
			
		}
		Vector2D Bullet::getBulletPosition()
		{
			return bulletPosition;
		}