#include "Matrix3D.h"
#include "Core.h"
#include "Planets.h"

float orbitAngle = .05f;

Planets::Planets(float x, float y, float drawingScale): Planetposition(x,y,0), velocity(10,10,0), vectorMag(drawingScale)
{
	orbitVectors[0] = Vector3D(-vectorMag,-vectorMag, 1);
	orbitVectors[1] = Vector3D(-vectorMag,+vectorMag,1);
	orbitVectors[2] = Vector3D(+vectorMag, +vectorMag,1);
	orbitVectors[3] = Vector3D(+vectorMag,-vectorMag,1);
}


Planets::Planets(Vector2D &point, float drawingScale) : Planetposition(point.x,point.y,0), velocity(10,10,0) , vectorMag(drawingScale)
{
	orbitVectors[0] = Vector3D(-vectorMag,-vectorMag, 1);
	orbitVectors[1] = Vector3D(-vectorMag,+vectorMag,1);
	orbitVectors[2] = Vector3D(+vectorMag, +vectorMag,1);
	orbitVectors[3] = Vector3D(+vectorMag,-vectorMag,1);
}


Planets::Planets(Vector3D &point, float drawingScale) :  Planetposition(point), velocity(10,10,0), vectorMag(drawingScale)
{
	orbitVectors[0] = Vector3D(-vectorMag,-vectorMag, 1);
	orbitVectors[1] = Vector3D(-vectorMag,+vectorMag,1);
	orbitVectors[2] = Vector3D(+vectorMag, +vectorMag,1);
	orbitVectors[3] = Vector3D(+vectorMag,-vectorMag,1);
}




void Planets::Draw(Core::Graphics& g)
{
	const unsigned int NUM_VECTORS= sizeof(orbitVectors) / sizeof(*orbitVectors);

	Matrix3D orbitPosition;
	Matrix3D translate =  translate.translate(orbitPosition.matrix[0][2] + Planetposition.x + 100, orbitPosition.matrix[1][2] +Planetposition.y+100); 
	Matrix3D rotate    =  rotate.rotate(orbitAngle);
	Matrix3D transform =   translate * rotate ; 	 

	
		for(int i = 0; i < NUM_VECTORS;i++)
		{

			const Vector3D &first =	 ( transform* (orbitVectors[i]));
			const Vector3D &second = ( transform * (orbitVectors[(i+1) % (NUM_VECTORS)]));

			
			g.DrawLine(first.x, first.y, second.x, second.y);			
			orbitAngle +=.001f;
		
		
		}
		
		
}

void Planets::Update(float dt)
{
	Matrix3D orbitPosition;
	Matrix3D translate =  translate.translate(orbitPosition.matrix[0][2] + Planetposition.x, orbitPosition.matrix[1][2] +Planetposition.y); 
	Matrix3D rotate    =  rotate.rotate(orbitAngle);

	Matrix3D transform =  translate  *rotate; 	 
	Vector3D acceleration(0, -1, 0);
	Vector3D direction(rotate *acceleration );
	dt *= 300;

	
	velocity = Vector3D(dt * rotate.matrix[0][0], rotate.matrix[1][1]);
	Planetposition = transform  * Planetposition;
	
}

void Planets::spawn(Vector3D position, int babies, float angle, Core::Graphics& g)
{
	if(babies  == -1)
	{
		return;
	}

	else
	{

		babies--;
	

		angle += .1f;

		Matrix3D transform =  transform.translate(position.x, position.y) *
			transform.rotate(orbitAngle + angle * orbitAngle) *transform.translate( 50.0f + (float)babies/2,0.0f);


		Planets planet(position,vectorMag + babies);

		Vector3D babyPosition = transform * Vector3D(1,1,1); 
		planet.Draw(g);

		
		spawn(babyPosition, babies, angle,g);
	}
}