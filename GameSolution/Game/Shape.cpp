#include "Shape.h"

	Shape::Shape(int lineCount,Vector2D* lineArray)
	{
		numLines=lineCount;
		lines=lineArray;
	}
	Shape::Shape()
	{}
	void Shape::Draw(Core::Graphics& graphics,Vector2D& where)
	{
		for(int i=0;i<numLines;i++)
		{
			graphics.DrawLine(where.x +lines[i].x,where.y+lines[i].y,where.x+lines[i+1].x,where.y+lines[i+1].y);
		}

	}
