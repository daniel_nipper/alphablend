#ifndef GameObject_H
#define GameObject_H
#include "Shape.h"
class GameObject: public Shape
{
public:
	Vector2D velocity;
	Vector2D position;
	Shape shape;

	GameObject(int numLines, Vector2D* outLine);
	void Draw(Core::Graphics& graphics);

};
#endif
