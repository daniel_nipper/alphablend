#ifndef Shape_H
#define Shape_H
#include "Matrix3D.h"
#include "Core.h"
class Shape
{
	int numLines;
	Vector2D* lines;
public:
	Shape(int lineCount,Vector2D* lineArray);
	Shape();
	void Draw(Core::Graphics& graphics,Vector2D& where);
};
#endif

