#include "Matrix3D.h"
#include "Core.h"
#include "Enemy.h"
Vector2D enemyShape[]={Vector2D(0,-10),Vector2D(-7,0),Vector2D(0,10),Vector2D(7,0),Vector2D(0,-10)};
Enemy::Enemy()
{undefined=0;}
Enemy::Enemy(Vector2D position): startPosition(position)
{
	undefined=1;
}

void Enemy::Update(Vector2D player)
{
	Vector2D toShip=player-startPosition;
	startPosition= startPosition + toShip.normalized();
}
void Enemy::Draw(Core::Graphics& graphics)
{
	graphics.SetColor(RGB(255,10,10));
	int numberLines = 4;
	Matrix3D enemyTranslate = enemyTranslate.translate(startPosition.x,startPosition.y);
	for(int i = 0; i < numberLines; i++)
	{
		Vector2D enemyStart = enemyTranslate * enemyShape[i];
		Vector2D enemyEnd = enemyTranslate * enemyShape[i+1];
		graphics.DrawLine(enemyStart.x,enemyStart.y,enemyEnd.x,enemyEnd.y);
	}
}
