#include "Game.h"
#include "MemoryDebug.h"
#include <sstream>
#include "ParticleEffect.h"

const int SCREEN_WIDTH=900;
const int SCREEN_HEIGHT=700;
using Core::RGB;
Vector2D asteroidShape[]={Vector2D(0,-10),Vector2D(-10,10),Vector2D(10,10),Vector2D(0,-10)};
Vector2D lifeSymbol[]={Vector2D(0,-5),Vector2D(-5,5),Vector2D(5,5),Vector2D(0,-5)};
bool show=false;
bool gameIsNotOver=true;
int setUp=0;
int gameStart=0;
float mover=10;
GameObject* lifeShip[5]={new GameObject(3,lifeSymbol),new GameObject(3,lifeSymbol),new GameObject(3,lifeSymbol),
	new GameObject(3,lifeSymbol),new GameObject(3,lifeSymbol)};
Turret one(3);
int cleanUp=0;
Vector2D use=Vector2D(float(SCREEN_WIDTH/2),float(SCREEN_HEIGHT/2));
Planets planets(use,2);
ParticleEffect stars=ParticleEffect(Vector2D(0,500),RGB(160,240,137),500,2,1);
Clock clock;

Game::Game()
{


}
Game::~Game()
{
	for(int i=0; i<5;i++)
	{
		delete lifeShip[i];
	}
}
std::string num2str(float num) {
	std::stringstream ss;
	ss << num;
	return ss.str();
};
void Game::Update(float dt)
{ 
	if(Core::Input::IsPressed(13))
	{
		show=true;
		if(gameStart==0)
		{
			/*LOG(Info,"game started");*/
			gameStart++;
		}
	}
	if(show && gameIsNotOver)
	{
		
		profiler.newFrame();
		{
			PROFILE("Turret upadate");
			one.Update(dt);
		}


stars.Update(dt);
	}

}
void Game::Draw(Core::Graphics& graphics)
{	
	if(setUp==0)
	{
		for(int i=0; i<5;i++)
		{
			lifeShip[i]->position=Vector2D(10+mover,180);
			mover+=20;
			setUp++;
		}
	}
	clock.initialize();
	if(show && gameIsNotOver)
	{
		
		for(int i=0; i<one.NumOfLives;i++)
		{
			lifeShip[i]->Draw(graphics);
		}
		if(one.NumOfLives>5)
		{
			/*LOG(Warning,"they cheated");*/
		}
		int length= 10;
		{
			PROFILE(" Plantes draw");
			planets.spawn(Vector3D(150,150,150),7,.2f,graphics);
		}
		{
			PROFILE("Tuuret draw");
			one.draw(graphics);
		}

		stars.Draw(graphics);
		graphics.SetColor(RGB(255,255,0));
		graphics.DrawString(length, length, "Move forward: W key");
		graphics.DrawString(length, length +20, "rotating planets are for decoration only");
		graphics.DrawString(length, length +40, "shoot: left mouse click and diamonds are enemies");
		graphics.DrawString(length, length + 60, "rotate left with A and right with D");
		


		graphics.DrawString(length, length +80,  "FPS: ");
		graphics.DrawString(length+40, length +80,  num2str(clock.miliSeconds()).c_str());

		graphics.DrawString(length, length +100,  "FPS recip: ");
		graphics.DrawString(length+80, length +100,  num2str(clock.lap()).c_str());
		graphics.DrawString(length,200, "count");
		graphics.DrawString(length+60,200,num2str((float)one.test1.lCounts[_CLIENT_BLOCK]).c_str());
		graphics.DrawString(length,220, "Sizes");
		graphics.DrawString(length+60,220,num2str((float)one.test1.lSizes[_CLIENT_BLOCK]).c_str());
		graphics.DrawString(length,240, "HighWaterCount");
		graphics.DrawString(length+120,240,num2str((float)one.test1.lHighWaterCount).c_str());
	}
	else
	{
		if(one.NumOfLives>=0)
		{

			graphics.DrawString(SCREEN_WIDTH/2-55,SCREEN_HEIGHT/2-20, "Press Enter To Begin");
		}
	}
	if(one.NumOfLives<0)
	{
		gameIsNotOver=false;
		graphics.DrawString(SCREEN_WIDTH/2-55,SCREEN_HEIGHT/2-10, "Game Over");
		if(cleanUp<5)
		{
			for(int i=0; i<5;i++)
			{
				delete lifeShip[i];
				cleanUp++;
			}
		}
	}
}



